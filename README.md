# NBA Scratchpad

Python/Flask application that serves up NBA Scratchpad, a html/Javascript/CSS-based tool for interacting with the Netherlands Biodiversity API.  

Scratchpad requires the following environment variables to be set:
- `PRIVATE_NBA_ADDRESS`: address of the NBA for requests from the proxy
- `PUBLIC_NBA_ADDRESS`: address of the NBA for use in the GUI

Optional variables:
- `NBA_VERSION`: id. Optional, defaults to 'v2'
- `NBA_TIMEOUT`: id. Optional, defaults to 10.
- `LISTENER_BASE_PATH`: relative path Scratchpad will run under. Optional, defaults to '/scratchpad'

To run:
```
python scratchpad.py
```

Scratchpad usage:
- Run as specified above.
- Access NBA Scratchpad: `http://<SERVER ADDRESS>:80/scratchpad/`
- F2 opens help file
- Starting Scratchpad with an external query (useful for linking example from documentation):  `http://<SERVER ADDRESS>:80/scratchpad/?_querySpec=<query>`


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`

