import os
import requests, urllib, json, httplib
from flask import Flask, request, make_response, render_template, stream_with_context, Response
from urllib import unquote_plus
# from flask_cors import CORS

# PRIVATE_NBA_ADDRESS is the docker-address of the NBA-container, including port, excluding protocol
private_nba_address = os.getenv('PRIVATE_NBA_ADDRESS','nba-api')
# PUBLIC_NBA_ADDRESS is the regular NBA-address, including port, excluding protocol
public_nba_address = os.getenv('PUBLIC_NBA_ADDRESS','api.biodiversitydata.nl')
nba_version = os.getenv('NBA_VERSION','v2')
nba_request_timeout = int(os.getenv('NBA_TIMEOUT','10'))
listener_base_path = os.getenv('LISTENER_BASE_PATH','/scratchpad').rstrip('/')
force_get_requests = os.getenv('FORCE_GET_REQUESTS','false').lower()=='true'

# internal docker communication is http
base_url = 'http://' + private_nba_address

app = Flask(__name__, static_url_path=listener_base_path+"/static")

print("private_nba_address: {}".format(private_nba_address))
print("public_nba_address: {}".format(public_nba_address))
print("nba_version: {}".format(nba_version))
print("nba_request_timeout: {}".format(nba_request_timeout))
print("listener_base_path: {}".format(listener_base_path))
print("force_get_requests: {}".format(force_get_requests))


@app.route(listener_base_path + '/', methods=['GET'])
def root_request():
	predef_query=request.args.get('_querySpec', '')
	execute_query=request.args.get('execute_query', '')
	strip_url=request.args.get('strip_url', '')
	return render_template('index.html',
		nba_address=public_nba_address,
		nba_version=nba_version,
		predef_query=predef_query,
		execute_query=execute_query,
		strip_url=strip_url)

@app.route(listener_base_path + '/url_test', methods=['POST'])
def url_test_request():
	s = []
	try:
		if 'data' in request.form:
			for value in json.loads(request.form['data']):
				r = requests.head(url=value['url'],timeout=nba_request_timeout)
				s.append({"id":value['id'],"status":r.status_code})
	except Exception as e:
		pass

	response = make_response(json.dumps(s))
	response.headers['content-type'] = "application/json"
	return response


@app.route(listener_base_path + '/proxy', methods=['GET','POST'])
def proxy_request():
	if request.method == "POST" and 'method' in request.form:
		this_method = request.form['method']
	else:
		this_method = request.method

	payload = {}

	if 'data' in request.form:

		for name, value in json.loads(request.form['data']).iteritems():
			try:
				payload[name] = unquote_plus(value)
			except Exception as e:
				payload[name] = value

	explain_level = False

	if '__explain' in request.form:
		if json.loads(request.form['__explain']):
			explain_level = json.loads(request.form['__explain'])

	try:
		this_url = base_url+request.form['service']+("?__explain&__level="+explain_level if explain_level else "")

		# print(this_url)

		if this_method == 'POST' and not force_get_requests == True:
			# r = requests.post(url=this_url,data=payload,timeout=nba_request_timeout,headers={ "Content-Type" : "application/x-www-urlencoded" })
			r = requests.post(url=this_url,data=payload,timeout=nba_request_timeout)
		else:
			r = requests.get(url=this_url,params=payload,timeout=nba_request_timeout)

		response = make_response(r.content)
		response.headers['content-type'] = r.headers.get('content-type')

		# for header, value in r.headers.iteritems():
		# 	print(header, value)

		# errors other than 300 and 400's are sent to the browser as-is
		if 300 <= r.status_code < 500:
			if not (r.text == None or r.text==""):
				s = r.text.replace("\n",": ")
			else:
				s = "{} ({})".format(str(r.status_code),httplib.responses[r.status_code])
			raise ValueError(s)

		if (r.headers.get('content-type')=='application/zip'):
			response.headers['content-disposition'] = r.headers.get('content-disposition')

		return response

	except Exception as e:
		err = { 'scratchpad_proxy_error' :
				{
					'error' : str(e),
					'nba_url' : this_url,
					'payload' : payload,
					'method' : this_method,
					'force_get_requests' : force_get_requests
				}
			}
		response = make_response(json.dumps(err))
		response.headers['content-type'] = "application/json"
		return response


@app.route('/', methods=['GET'])
def base():
	return render_template('root.html',scratchpad_path=listener_base_path+'/')


if __name__ == '__main__':
	FLASK_PORT = int(os.getenv('FLASK_PORT',80))
	app.run(debug=(os.getenv('FLASK_DEBUG')=="1"),host='0.0.0.0', port=FLASK_PORT)
