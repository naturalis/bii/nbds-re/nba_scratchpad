# NBA Scratchpad Changelog
## v3.11 (2020.11.09)

### New features
- Added service summaries as mouseover-title in the services/endpoint dropdown.
- 'Format query', properly formats your query, making it much more readable (it takes up quite a lot of space in this format; clicking 'format query' while holding down Ctrl applies a more compact format). Please note that formatting won’t work if there are comment lines in the query window.
- Added total result size. Above the result window on the right side, the total size of the current results is displayed (so you don’t have to scroll down when they appear at the bottom of the results). Also works for result sets that doen't actually include a total size.
- The newly introduced NameResolutionRequest has an explain-option, which allows you to gain insight in how the service compiles its results. When one of these endpoints is selected, a checkbox with the &#8265;-icon appears. Checking the checkbox changes the output to the details of the resolution process. You can choose the detail level of this explanation under ‘more options’.
- Scratchpad detects BatchQuery-results, and automatically constructs the query for the next batch, which can be executed with the “next batch” button. Once you’ve reached the lastbatch, the button automatically disappears.
- Added result download-option, including the option of custom name with an auto-incrementing counter (hold Ctrl while clicking ‘download’ for options).
- User can explicitly select the request method used in the request (GET or POST).
- Correct handling of new abstracted services (that use {type} rather than specimen, taxon etc.) in the NBA endpoint list.
- Added searchType-operators for nameResolutionRequest to query window context menu.


### Improvements
- Context menu (‘right click’-menu) in query window now has proper “insert into query”-options. Rather than just pasting bits of query, all options under ‘insert in existing query’ fit the corresponding clause correctly into the query, making sure the query remains a valid object. If you do this while the window is empty, the first insert will no just add the element you’ve clicked, but will make sure the bare bones of a complete query are also added. Note that this won’t work if there are comment lines in the query window.
- Tidied service list of endpoints labels (please note: the endpoints that were labeled as “human readable” endpoints are now listed with an &#128065; icon.
- Result window now fits the screen without scrolling.
- Long saved query lists can now be scrolled.
- ▼-icon replaced with 'more options'
- Replaced original example queries with a set of more illustrative examples. Instructions for loading:
  - if you open scratchpad and there are no saved queries yet, it automatically creates a group 'EXAMPLES' with a set of examples in it;
  - if you do have saved queries (such as the old example queries), you can throw them away (delete queries with the x after each query’s name, or all at once via 'more options'). Once you’ve deleted the last one, scratchpad automatically creates the new examples;
  - if you just want the new example queries, and don't want to delete your existing saved queries, there’s an option '(re) import example queries' under  'more options'.
  - Moved 'add slashes' to query window context menu.
- For non-JSON responses, replaced iFrame with html-object.
- Fixed 'always GET'-bug in the scratchpad-proxy.
- Increased speed of loading by compression on all traffic from and to the scratchpad-proxy.


### Removed features
- Removed option for PURL-requests.
- Took out “access keys” for various page elements (patchy browser support). 
