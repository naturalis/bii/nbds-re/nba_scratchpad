class QueryWindow extends Base {

	selector_query = '#query'
	selector_context_menu = '#queryWindowContextMenu'
	selector_cursor_position = '#cursor-position'
	selector_query_trigger = '#query-trigger'

	contextmenuActive = true
	loadedExternalQuery = false

	text = null
	startPos = null
	endPos = null
	textBeforeSelectedText = ""
	selectedText = ""
	textAfterSelectedText = ""

	fullRequestUrl = ""

    constructor(config,server)
    {
        super(config);
        this.setServer(server);
    }

	setText()
	{
		this.text = $(this.selector_query).val();
	}

	setQueryWindowValue(val)
	{
		$(this.selector_query).val(val);
	}

	getText()
	{
		return this.text;
	}

	setSelectedText()
	{
	    this.startPos = $(this.selector_query).prop('selectionStart');
	    this.endPos   = $(this.selector_query).prop('selectionEnd');

		if (this.startPos!=this.endPos)
		{
			this.textBeforeSelectedText = $(this.selector_query).val().substring(0,this.startPos);
			this.selectedText = $(this.selector_query).val().substring(this.startPos,this.endPos);
			this.textAfterSelectedText = $(this.selector_query).val().substring(this.endPos);
		}
		else
		{
			this.textBeforeSelectedText = "";
			this.selectedText = "";
			this.textAfterSelectedText = "";
		}
	}

	getSelectedText()
	{
		return this.selectedText;
	}

    setQueryHint()
    {
        var tpl = "";

        if (!this.service)
        {
        	return;
        }

		if (this.service.pathParams.length==1)
		{
			tpl = this.getTemplate("queryHintField_tpl");

			if (this.service.acceptsQueryBody)
			{
				tpl = tpl + "\n" + this.getTemplate("queryOptional_tpl");
			}
		}
		else
		if (this.service.pathParams.length==2)
		{
			tpl = this.getTemplate("queryHintGroupField_tpl");

			if (this.service.acceptsQueryBody)
			{
				tpl = tpl + "\n" + this.getTemplate("queryOptional_tpl");
			}
		}
		else
		if (this.service.acceptsQueryBody)
		{
			if (this.service.mandatoryQueryBody)
			{
				tpl = this.getTemplate("queryHintQuery_tpl");
			}
			else
			{
				tpl = this.getTemplate("queryOptional_tpl");
			}
		}
		else
		if (this.service.acceptsQueryString)
		{
			tpl = this.getTemplate("queryHintSimpleQuery_tpl");
		}

        $(this.selector_query).attr("placeholder",tpl);
    }

    setTabularResults(tabular_results)
    {
    	this.tabular_results = tabular_results;
    }

	setContextmenuActive(state)
	{
		this.contextmenuActive=state;
	}

	getContextmenuActive()
	{
		return this.contextmenuActive;
	}

	setFullRequestUrl(fullRequestUrl)
	{
		this.fullRequestUrl = fullRequestUrl;
	}

	insertString( str )
	{
		var startPos = $(this.selector_query).prop('selectionStart');
		var endPos = $(this.selector_query).prop('selectionEnd');
		if (startPos!=endPos)
		{
			$(this.selector_query).val($(this.selector_query).val().substring(0,startPos)+$(this.selector_query).val().substring(endPos));
		}
		var val = $(this.selector_query).val();
		$(this.selector_query).val(val.substring(0,startPos) + str + val.substring(startPos,val.length));
		this.focusWindow();
		$(this.selector_query).selectRange(startPos+str.length);
	}

	focusWindow()
	{
		$(this.selector_query).focus();
	}

	setContextMenu(pageX,pageY)
	{
		var main=[];
		var sub=[];

		/* query menu */
		var buffer=[];
		buffer.push(this.getTemplate('codeBitContextHeaderTpl').replace('%LABEL%','insert in existing query'));

		Array.from(this.config.insertableElements).forEach(arg =>
		{
			buffer.push(this.getTemplate('buttonContextTpl').replace('%LABEL%',arg.label).replace('%TEXT%',arg.label));
		});

		if (this.tabular_results.getCurrentResultView()==this.tabular_results.VIEW_TYPE_TABULAR)
		{
			buffer.push(this.getTemplate('context_useTabularHeadersAsFieldsTpl'));
		}

		buffer.push(this.getTemplate('codeBitContextHeaderTpl').replace('%LABEL%',''));
		buffer.push(this.getTemplate('codeBitContextHeaderTpl').replace('%LABEL%','full queries'));

		var tpl = this.getTemplate('codeBitContextTpl');

		Array.from(this.config.codeBits).forEach(function(arg,i)
		{
			buffer.push(tpl.replace('%I%',i).replace('%LABEL%',arg.label));
		},tpl);

		main.push(
			this.getTemplate('queryContextMainItemTpl')
				.replace(/%LOCAL_ID%/gm,main.length)
				.replace('%LABEL%','queries')
				.replace('%TITLE%','queries')
		);

		sub.push(
			this.getTemplate('queryContextItemsTpl')
				.replace(/%LOCAL_ID%/gm,main.length-1)
				.replace('%ITEMS%',buffer.join("\n"))
		);

		/* operators menu */
		var buffer=[];

		buffer.push(this.getTemplate('codeBitContextHeaderTpl').replace('%LABEL%','regular operators'));

		var tpl = this.getTemplate('queryOperatorContextTpl');
		Array.from(config.queryOperators).forEach(arg =>
		{
			buffer.push(tpl.replace(/%OPERATOR%/g,arg));
		},tpl);

		buffer.push(this.getTemplate('codeBitContextHeaderTpl').replace('%LABEL%',''));
		buffer.push(this.getTemplate('codeBitContextHeaderTpl').replace('%LABEL%','nameResolutionRequest searchType'));

		var tpl = this.getTemplate('queryOperatorContextTpl');
		Array.from(config.nameResolutionRequestSearchTypes).forEach(arg =>
		{
			buffer.push(tpl.replace(/%OPERATOR%/g,arg));
		},tpl);

		main.push(
			this.getTemplate('queryContextMainItemTpl')
				.replace(/%LOCAL_ID%/gm,main.length)
				.replace('%LABEL%','operators')
				.replace('%TITLE%','operators')
		);

		sub.push(
			this.getTemplate('queryContextItemsTpl')
				.replace(/%LOCAL_ID%/gm,main.length-1)
				.replace('%ITEMS%',buffer.join("\n"))
		);

		/* context edit menu */
		main.push(
			this.getTemplate('queryContextMainItemTpl')
				.replace(/%LOCAL_ID%/gm,main.length)
				.replace('%LABEL%','edit')
				.replace('%TITLE%','edit')
		);

		var buffer=[];
		var tpl = this.getTemplate('editContextTpl');

		Array.from(
			[
				{ 'label' : 'copy', 'action' : 'query_window.copyString();query_window.closeContextMenu();' },
				{ 'label' : 'cut', 'action' : 'query_window.cutString();query_window.closeContextMenu();' },
				{ 'label' : 'clear query', 'action' : 'query_window.clear();query_window.closeContextMenu();' },
				{ 'label' : 'apply comment', 'action' : 'query_window.addCommentChars();query_window.closeContextMenu();' },
				{ 'label' : 'remove comment', 'action' : 'query_window.removeCommentChars();query_window.closeContextMenu();' },
				{ 'label' : 'add slashes', 'action' : 'query_window.addSlashes();query_window.closeContextMenu();' },
				{ 'label' : 'add double quotes', 'action' : 'query_window.addDoubleQuotes();query_window.closeContextMenu();' },
			]
		).forEach(arg => {
			buffer.push(
				tpl
					.replace('%LABEL%',arg.label)
					.replace('%TITLE%',arg.label)
					.replace('%ONCLICK%',arg.action)
			);
		},tpl);

		sub.push(
			this.getTemplate('queryContextItemsTpl')
				.replace(/%LOCAL_ID%/gm,main.length-1)
				.replace('%ITEMS%',buffer.join("\n"))
		);

		$(this.selector_context_menu)
			.css({
				'position':'absolute',
				'left':  (pageX+5) + 'px',
				'top': (pageY+10) + 'px'
			})
			.html(main.join(" | ")+sub.join(""))
			.toggle(true);
	}

	bootstrapContextmenu()
	{
		if (!this.contextmenuActive)
		{
			return;
		}

		$(this.selector_query).on('contextmenu',
		{
			caller: this
		},
		function(e)
		{
			e.preventDefault();
			e.data.caller.setContextMenu(e.pageX,e.pageY);
		});
	}

	closeContextMenu()
	{
		$(this.selector_context_menu).toggle(false);
	}

	onChange()
	{
		this.setText();
		this.setSelectedText();
		this.showCaretPosition();
	}

	bootstrapWindow()
	{
		$(this.selector_query)
			.on('change', { caller: this },
				function(e)
				{
					e.data.caller.onChange();
				})
			.on('click', { caller: this },
				function(e)
				{
					e.data.caller.onChange();
					e.data.caller.closeContextMenu();
				})
			.on('keyup', { caller: this },
				function(e)
				{
					var ctrlPressed = bindKeys.getCtrlPressed();
					var shiftPressed = bindKeys.getShiftPressed();

					if (e.keyCode==122 && ctrlPressed && shiftPressed) // ctrl+shift+F11
					{
						e.data.caller.removeCommentChars();
					}
					else
					if (e.keyCode==122 && ctrlPressed) // ctrl+F11
					{
						e.data.caller.addCommentChars();
					}
					else
					if (e.keyCode==123 && ctrlPressed) // ctrl+F12
					{
						e.data.caller.addDoubleQuotes();
					}
					else
					if (e.keyCode==32 && ctrlPressed) // ctrl+space
					{
						e.data.caller.insertString( String.fromCharCode(32).repeat(e.data.caller.config.ctrlSpaceNumberOfSpaces) );
					}

					e.data.caller.onChange();
				})
			.on('keydown', { caller: this },
				function(e)
				{
					e.data.caller.onChange();
				})
			.on('mouseup', { caller: this },
				function(e)
				{
					e.data.caller.onChange();
				});

		$(this.selector_cursor_position).html(this.getTemplate('cursorPositionTpl').replace('%LINE%',0).replace('%POS%',0));
	}

	copyString()
	{
		var s=this.getSelectedText();
		functions.copyToClipboard(s);
		this.setMessage('copied string to clipboard');
	}

	cutString()
	{
		var s=this.getSelectedText();
		if (this.startPos!=this.endPos)
		{
			$(this.selector_query).val(this.textBeforeSelectedText + this.textAfterSelectedText);
		}

		functions.copyToClipboard(s);
		this.setMessage('cut string to clipboard');
	}

	clear()
	{
		this.setQueryWindowValue("");
	}

	insertString( str )
	{
		if (this.startPos!=this.endPos)
		{
			this.setQueryWindowValue(
				$(this.selector_query).val().substring(0,this.startPos)+
				$(this.selector_query).val().substring(this.endPos)
			);
		}

		var val = $(this.selector_query).val();
		this.setQueryWindowValue(val.substring(0,this.startPos) + str + val.substring(this.startPos,val.length));
		this.focusWindow();
		$(this.selector_query).selectRange(this.startPos+str.length);
	}

	addCommentChars()
	{
		var def_cc = this.config.commentCharacters[this.config.defaultCommentCharIndex];
		var sel = this.getSelectedText();
		var commented = functions.addCommentCharToText(sel,def_cc);
		this.insertString(commented);
	}

	removeCommentChars()
	{
		var def_cc = this.config.commentCharacters[this.config.defaultCommentCharIndex];
		var sel = this.getSelectedText();
		var uncommented = functions.removeCommentCharFromText(sel,def_cc);
		this.insertString(uncommented);
	}

	addSlashes()
	{
		if (this.startPos!=this.endPos)
		{
			var str = this.getSelectedText();

			if (bindKeys.getCtrlPressed())
			{
				this.insertString( str.replace(/"/g, '\\"').replace(/\n/g, '').replace(/(\s)+/g, ' ') );
			}
			else
			if (bindKeys.getShiftPressed())
			{
				this.insertString( str.replace(/\\"/g, '"') );
			}
			else
			{
				this.insertString( str.replace(/"/g, '\\"') );
			}
		}
	}

	addDoubleQuotes()
	{
		var quoted = functions.doubleQuoteText(this.getSelectedText());
		this.insertString(quoted);
	}

	showCaretPosition()
	{
		var matches=[];
		matches=functions.findAllSubstrings($(this.selector_query).val(),"\n",0,matches);
		matches.push($(this.selector_query).val().length);

		var line=0;
		var pos=0;
		var tPos=this.startPos;

		for(var i=0;i<matches.length;i++)
		{
			line=i;
			pos=tPos-(i==0?0:matches[i-1]);
			if (tPos<=matches[i]) break
		}

		$(this.selector_cursor_position).html(
			this.getTemplate('cursorPositionTpl')
				.replace('%LINE%',line+1)
				.replace('%LINE_POS%',pos)
				.replace('%POS%',this.startPos)
			);
	}

	insertOperator( operator )
	{
		this.insertString(operator);
		this.focusWindow();
	}

	insertCodeBit( ele )
	{
		var codeBit = "";

		if ("object" == typeof(ele))
		{
			if (this.config.codeBits[$( ele ).attr("value")].template)
			{
				codeBit = this.getTemplate(config.codeBits[$( ele ).attr("value")].template);
			}
			else
			{
				codeBit = this.config.codeBits[$( ele ).attr("value")].code;
			}
		}
		else
		{
			var a = this.config.codeBits.filter(function(a)
			{
				return a.altGr==ele;
			});

			if (a[0] && a[0].template)
			{
				codeBit = this.getTemplate(a[0].template);
			}
			else
			if (a[0] && a[0].code)
			{
				 codeBit = a[0].code;
			}
		}

		this.insertString( codeBit + "\n" );
		this.focusWindow();
	}

	insertQueryElement(label,element=null)
	{

		label = label ?? "condition";

		if (label!=="custom")
		{
			var ele = Array.from(this.config.insertableElements).filter(arg =>
			{
				return arg["label"] == label;
			});

			element = ele[0];
		}

		if (element)
		{
			if (element.template)
			{
				var code = this.getTemplate(element.template);
			}
			else
			{
				var code = element.code;
			}

			code = code.replace(/,$/g,'');

			try {

				JSON.parse(code);

			} catch (error)
			{
				code = "{"+code+"}";
			}
		}
		else
		{
			return;
		}

		var prop = element.property;

		try
		{
			var q = $(this.selector_query).val().trim();
			var o = JSON.parse(q.length>0 ? q :"{}");

			if (o[prop])
			{
				if (element.repeatable)
				{
					o[prop][o[prop].length]=JSON.parse(code);
				}
				else
				{
					o[prop]=JSON.parse(code);
				}
			}
			else
			{
				if (element.repeatable)
				{
					o[prop]=[];
					o[prop][0]=JSON.parse(code);
				}
				else
				{
					o[prop]=JSON.parse(code);
				}
			}

			this.setQueryWindowValue(JSON.stringify(o, undefined, 4));

		} catch (error)
		{
			this.setMessage("illegal JSON, cannot insert")
			// console.error(error);
		}

	}

	triggerQueryClick()
	{
		$(this.selector_query_trigger).trigger('click');
	}

	loadExternalQuery(query,strip_url)
	{
		this.loadedExternalQuery=false;

		// stripping JSON outer double-quotes (and spaces)
		query=query.replace(/(^[\s"]*|[\s"]*$)/g, "");

		if (strip_url)
		{
			// altering the URL without reloading (for bookmarking purposes)
			var stateObj = { foo: "bar" };
			window.history.pushState(stateObj, "" , "/scratchpad/" );
		}

		if (query.length==0) return;

		if ($(this.selector_query).val().length==0)
		{
			this.setQueryWindowValue( query );
			this.loadedExternalQuery=true;
		}
	}

	executeExternalQuery(state)
	{
		if (this.loadedExternalQuery && state)
		{
			this.triggerQueryClick();
		}
	}

	tidyText()
	{
		// remove superfluous comma's (result from code-snippet insert)
		this.setQueryWindowValue(this.text.replace(/(,)(\s*)([\]\}])/g, function(match, p1, p2, p3, offset, string)
	    {
	       return p2+p3;
	    }).trim());
	}

	replaceEmptyText()
	{
		if (this.text.length>0)
		{
			return;
		}

		var tpl = "";

		if (this.service.pathParams.length==1)
		{
			tpl = this.getTemplate('emptyFieldQueryTpl');
		}
		else
		if (this.service.pathParams.length==2)
		{
			tpl = this.getTemplate('emptyGroupFieldQueryTpl');
		}
		else
		if (this.service.acceptsQueryBody)
		{
			tpl = this.getTemplate('emptyQueryTpl');
		}

		this.setQueryWindowValue(tpl);
		$(this.selector_query).trigger("change");
	}

	decodeQuery()
	{
		if (bindKeys.getCtrlPressed())
		{
			this.encodeQuery();
		}
		else
		{
			this.setQueryWindowValue( decodeURIComponent( this.text ) );
		}
	}

	encodeQuery()
	{
		this.setQueryWindowValue( encodeURIComponent( this.text ) );
	}

	formatQuery()
	{
		var unformat = bindKeys.getCtrlPressed();
		var q = $(this.selector_query).val();

		try {

			var sel = this.getSelectedText();

			if (sel.length>0)
			{
				if (unformat)
				{
					this.insertString(this.unformatString(sel));
				}
				else
				{
					this.insertString(JSON.stringify(JSON.parse(sel), undefined, 4));
				}

				$(this.selector_query).scrollTop(0);
			}
			else
			{
				if (unformat)
				{
					this.setQueryWindowValue(this.unformatString(q));
				}
				else
				{
					this.setQueryWindowValue(JSON.stringify(JSON.parse(q), undefined, 4));
				}
			}

		} catch (error) {

			try {
				JSON.parse(functions.removeCommentsFromText(q,this.config.commentCharacters));
				setMessage("cannot format with comments present;\ntry formatting selected text rather than the whole window",false);
			} catch (e2)
			{
				console.error(e2);
			}

		}
	}

	unformatString(x)
	{
		var r = x.replace(/[\s]+/g," ");

		r = r.replace(/"(fields|logicalOperator|sortFields)"/gi, function (x)
		{
			return "\n  " + x;
		});

		r = r.replace(/"nameResolutionRequest"(.*?)}/smi, function (x)
		{
			return "\n  " + x.replace(/"(from|size|searchString|nameTypes|searchType|useCoL|fuzzyMatching)"/gi, function (x2)
			{
				return "\n   >>" + x2;
			});

		});

		r = r.replace(/"conditions"(.*?)]/smi, function (x)
		{
			return "\n  " + x.replace(/{ "field/gi,function (x2)
			{
				return "\n   >>" + x2;
			});

		});

		r = r.replace(/[^>]{2}(from|size)/smgi,function (x)
		{
			return "\n  " + x.trim();
		});

		r = r.replaceAll(">>","  ");

		return r;
	}

	copyQueryToClipboard()
	{
		if (bindKeys.getCtrlPressed())
		{
			this.copyJiraQueryToClipboard();
			this.setMessage('copied query for JIRA to clipboard');
		}
		else
		{
			functions.copyToClipboard($(this.selector_query).val().trim());
			this.setMessage('copied query to clipboard');
		}
	}

	copyJiraQueryToClipboard()
	{
		functions.copyToClipboard(this.getTemplate( 'jiraNoFormatTpl' ).replace('%QUERY%',$(this.selector_query).val().trim()).trim());
	}

	copyCompleteRequestToClipboard()
	{
		functions.copyToClipboard(
			this.getTemplate( 'completeRequestExportTpl' )
				.replace('%SERVER%',this.server.url)
				.replace('%SERVICE%',this.service.path)
				.replace('%SERVICE_LABEL%',this.service.label)
				.replace('%QUERY%',$(this.selector_query).val())
				.replace('%URL%',this.fullRequestUrl)
		);
	}

	queryWindowContextToggle()
	{
	    if (this.getContextmenuActive())
	    {
	        $(this.selector_query).off('contextmenu');
	        this.setContextmenuActive(false);
	        this.setMessage('query window context menu off');
	    }
	    else
	    {
	        this.setContextmenuActive(true);
	        this.bootstrapContextmenu();
	        this.setMessage('query window context menu on');
	    }
	}

    setCursorAtPosition(pos)
    {
		$(this.selector_query).focus();
	    $(this.selector_query).prop('selectionStart',pos);
	    $(this.selector_query).prop('selectionEnd',pos);

    }
}