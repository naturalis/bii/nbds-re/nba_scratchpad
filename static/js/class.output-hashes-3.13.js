class OutputHashes extends Base {

	hashes =  Array()
	visible = false
	counter = 0

	store( output )
	{
		if (output)
		{
			var d = new Date();
			this.hashes.push( { id: this.counter++, hash: md5(output), timestamp: d.getTime() } );
		}
	}

	remove( i )
	{
		if (i)
		{
			var n = this.hashes.findIndex(function(a, index, arr) { return a.id==i; }, i);
			this.hashes.splice(n,1);
		}
		else
		{
			this.hashes=[];
			this.counter=0;
		}
	}

	toggle()
	{
		if (!this.visible) this.remove();
		$('.hashes').toggle(!this.visible);
		this.visible=!this.visible;
		if (this.visible) this.print();
		this.setMessage('output hashes ' + (this.visible ? 'on' : 'off') );
	}

	print()
	{
		var buffer=[];

		for(var i=0;i<this.hashes.length;i++)
		{
			var d = new Date(this.hashes[i].timestamp);
			buffer.unshift(
				this.getTemplate('hashTpl')
					.replace('%I%',this.hashes[i].id)
					.replace('%HASH%',this.hashes[i].hash)
					.replace('%TIMESTAMP%',d.toLocaleTimeString())
					.replace('%CLASS%',(this.hashes[i-1] && this.hashes[i].hash!=this.hashes[i-1].hash ? 'diff' : '' ))
				);
		}

		$('#hashes').html(this.getTemplate('hashesTpl').replace(/%ITEMS%/g,buffer.join("\n")));
	}

}