class QueryHistory extends Base {

    selector_list_container = "#query-session-list-container"

    id = 0
    stack = Array()
    stack_max = 100
    display_buffer = Array()

    storeQuery(query,endpoint_key,label)
    {
        if (this.stack.length>0)
        {
            var last = this.stack[0];

            if (last.query == query && last.endpoint_key == endpoint_key)
            {
                return;
            }
        }

        this.stack.unshift({"id":this.id++,"query":query,"endpoint_key":endpoint_key,"label":label,"timestamp":Date.now()});
    }

    capStack()
    {
        if (this.stack.length>this.stack_max)
        {
            this.stack = this.stack.slice(-this.stack_max);
        }
    }

    setDisplayBuffer()
    {
        this.display_buffer = []

        var tpl = this.getTemplate("queryListItemTpl");
        var cutoff_length = 200;

        Array.from(this.stack).forEach(arg=>
        {
            var d = new Date(arg.timestamp)
            var q = arg.query.replace(/(\s)+/," ");
            this.display_buffer.push(
                tpl.replace("%LABEL%",arg.label)
                .replace("%H%",functions.leftPad(d.getHours(),2,"0"))
                .replace("%M%",functions.leftPad(d.getMinutes(),2,"0"))
                .replace("%S%",functions.leftPad(d.getSeconds(),2,"0"))
                .replace("%QUERY%",arg.query.replace(/"/g, '&#34;'))
                .replace("%QUERY_FULL%",arg.query)
                .replace("%QUERY_HINT%",q.substring(0,cutoff_length) + (q.length > cutoff_length ? "&hellip;" : "" ))
                .replace(/%ID%/g,arg.id)
            );
        });

        if (this.display_buffer.length==0)
        {
            this.display_buffer.push("found no queries.");
        }
    }

    showHistory()
    {
        this.setDisplayBuffer();
        var list = this.getTemplate("queryListTpl").replace("%BODY%",this.display_buffer.join("\n"))

        functions.dialog(
            "recently executed queries",
            this.getTemplate("querySessionDialogBodyTpl").replace("%LIST%",list),
            null,
            null,
            {
                width : "60%",
                buttons: [ { text: "close", click: function() { $( this ).dialog( "close" ); } } ],
                height: "auto"
            }
        );
    }

    getItem(id)
    {
        var this_query = this.stack.filter(function(arg){return arg.id==id});

        if (this_query.length>0)
        {
            return this_query[0];
        }
    }

    removeItem(id)
    {
        var new_stack = [];

        Array.from(this.stack).forEach(arg=>
        {
            if (arg.id != id)
            {
                new_stack.push(arg);
            }
        });
        this.stack = new_stack;
    }

    removeAll()
    {
        this.stack = Array();
    }

    refreshList()
    {
        this.setDisplayBuffer();
        $(this.selector_list_container).html(this.getTemplate("queryListTpl").replace("%BODY%",this.display_buffer.join("\n")));
    }

}