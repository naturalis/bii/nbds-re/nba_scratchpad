class Request extends Base {

	config = null
	query = null
	queryEncoded = null
	customUrl = null
	pathParams = null
	requestObject = {}

    constructor(config,server)
    {
        super(config);
        this.setServer(server);
    }

	setQuery(query)
	{
		this.query = query;
	}

	setQueryEncoded(queryEncoded)
	{
		this.queryEncoded = queryEncoded;
	}

	setCustomUrl(customUrl)
	{
		this.customUrl = customUrl;
	}

    setPathParams(pathParams)
    {
        this.pathParams = pathParams;
    }

	setRequestObject()
	{
		if(this.service.forceNewWindow)
		{
			var serverUrl = this.server.nbaServer;
		}
		else
		{
			var serverUrl = this.server.url;
		}

		if (this.service.port)
		{
			var u=document.createElement('a');

			u.href = this.server.url;

			if (u.port)
			{
				serverUrl = serverUrl.replace(":"+u.port,":"+this.service.port)
			}
			else
			{
				serverUrl = serverUrl.trim().replace(/\/$/, "") + ":"+this.service.port;
			}
		}

		// if a service can handle both POST and GET, we use POST
		var method = this.service.method.indexOf("POST")!==-1 ? "POST" : "GET";
		var path = this.service.path;
		var payload = {};
		var query = "";

		if (!this.service.noQuery)
		{
			if (this.pathParams)
			{
				path = path + "/" + this.pathParams.join("/");
			}

			// POST first!
			if (this.service.acceptsQueryBody)
			{
				payload["_querySpec"]=this.query;
				query = this.queryEncoded ? "/?_querySpec=" + this.queryEncoded : "";
			}
			else
			if (this.service.acceptsQueryString)
			{
				Array.from(this.query.split("&")).forEach(arg =>
				{
					var keyVal = arg.split("=");
					if (keyVal[1])
					{
						payload[keyVal[0]] = keyVal[1];
					}
				})

				query = "/?" + this.query;
			}
		}

		this.requestObject = {
			"method" : method,
			"fullRequestUrl" : serverUrl + path + query,
			"servicePath" : path,
			"payload" : payload,
			"__explain" : false
		}

		if (this.customUrl)
		{
			payload = {};

			var customPath = this.customUrl.replace(serverUrl,"");
			var parts = customPath.split("?");

			if (parts[1])
			{
				Array.from(parts[1].split("&")).forEach(arg =>
				{
					var key = arg.split("=")[0];
					var val = arg.split("=")[1];
					if (val)
					{
						payload[key] = val;
					}
				})
			}

			this.requestObject.fullRequestUrl = this.customUrl;
			this.requestObject.servicePath = customPath.split("?")[0];
			this.requestObject.payload = payload;

			var matched_explain = this.requestObject.fullRequestUrl.match(/__explain/);
			var matched_level = this.requestObject.fullRequestUrl.match(/__level\=[a-z]+/);

			if (matched_explain!=null && matched_level!=null)
			{
				this.requestObject.__explain = matched_level[0].split("=")[1];
			}
			else
			{
				if ($("#addExplain").prop("checked") && this.service.explainable)
				{
					this.requestObject.__explain = $('#explainLevel').val();
					this.requestObject.fullRequestUrl += "&__explain&__level="+$('#explainLevel').val();
				}
			}
		}
		else
		{
			if ($("#addExplain").prop("checked") && this.service.explainable)
			{
				this.requestObject.__explain = $('#explainLevel').val();
				this.requestObject.fullRequestUrl += "&__explain&__level="+$('#explainLevel').val();
			}
		}

		if($("input[name='force-method']:checked").val()!="auto")
		{
			this.requestObject.method=$("input[name='force-method']:checked").val();
		}
	}

	setCompleteRequestObject(requestObject)
	{
		this.requestObject = requestObject;
	}

	getRequestObject()
	{
		return this.requestObject;
	}

	setResponseStatus(status)
	{
		$('#serverResponseCode').html(status);
	}

	doRequest()
	{
		// download & dwca services
		if (this.service.forceNewWindow || this.service.produces=='application/zip')
		{
			window.open(this.requestObject.fullRequestUrl,'_new');
			return;
		}

		this.timer.reset( this.timer.QUERY_EXECUTION );
		this.timer.checkpoint( this.timer.QUERY_EXECUTION, 'launch URL' );

		this.setResponseStatus(this.requestObject.method);

		var data = {
			method: this.requestObject.method,
			service: this.requestObject.servicePath,
			data: JSON.stringify(this.requestObject.payload),
			__explain: JSON.stringify(this.requestObject.__explain)
		};

		var p={
			method: "POST",
			url: this.server.proxyUrl + this.server.proxyPath,
			data: data,
			success: function(data, status, xhr)
			{
				var contentType = xhr.getResponseHeader("content-type").replace(/;(\s*)charset\=UTF-8/i,'');
				main.processRequestResult({
					data: {
						data: data,
						contentType: contentType,
						localresults: (contentType!="text/html"),
						formatted: null,
						tsv: null,
						log: null,
					},
					status: xhr.status
				});
			},
			error: function (request, status, error)
			{
				main.processRequestError(request);
			}
		};

		$.ajax( p );
	}
}