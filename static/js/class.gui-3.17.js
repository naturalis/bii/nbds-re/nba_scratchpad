class GUI extends Base
{
    class_add_explain = '.add-explain'
    class_doc_field_incl_extras = '.document-fields-include-extras'
    class_doc_field_show_all = '.document-fields-show-all'
    class_enable_backup = '.enable-backup'
    class_enable_ctrl_save = '.enable-ctrl-s'
    class_explain_group = '.explain-group'
    class_query_export = '.query-export'
    class_quick_export = '.quick-export'
    class_save_with_service = '.save-with-service'
    selector_add_explain = '#addExplain'
    selector_doc_field_incl_extras = '#documentFieldsIncludeExtras'
    selector_doc_field_show_all = '#documentFieldsShowAll'
    selector_enable_backup = '#enableMonthlyBackup'
    selector_enable_ctrl_save = '#enableCtrlS'
    selector_help = '#helpDiv';
    selector_prefix_export = '#export-'
    selector_prefix_group = '#group-'
    selector_prefix_group_export = '#group-export-'
    selector_result_query = '#resultQuery'
    selector_result_search = '#resultSearch'
    selector_run_altered_query = '#run-altered-query'
    selector_server_status = '#serverStatus'
    selector_file_input = '#file-input'
    selector_local_results = '#localresults'

    title_results_normal = "result window"
    title_results_full_screen = "press Esc to exit full screen"

    explainBlinked = false
    result_query_url = null

    constructor(config,server)
    {
        super(config);
        this.setServer(server);
    }

    setResultWindowUrl(url)
    {
        if (url)
        {
            this.result_query_url = url;
            $(this.selector_result_query).val(url);
            return;
        }

        if (this.server && this.service && this.server.url && this.service.path)
        {
            $(this.selector_result_query).val(this.server.url+this.service.path);
        }
        else
        {
            $(this.selector_result_query).val('error');
            this.setMessage("couldn't get service or service.path",false);
        }
    }

    getResultWindowUrl()
    {
        return $(this.selector_result_query).val();
    }

    setServerStatusLink()
    {
        if (this.server==undefined) return;
        $(this.selector_server_status).attr("href",this.server.testpath).toggle(( typeof this.server.testpath == 'string' ));
    }

    checkDocumentFieldsIncludeExtras()
    {
        $(this.class_doc_field_incl_extras+'.check-box-label-off').css('display',$(this.selector_doc_field_incl_extras).is(':checked') ? 'inline-block' : 'none');
        $(this.class_doc_field_incl_extras+'.check-box-label-on').css('display',$(this.selector_doc_field_incl_extras).is(':checked') ? 'none' : 'inline-block');
    }

    checkDocumentFieldsShowAll()
    {
        $(this.class_doc_field_show_all+'.check-box-label-off').css('display',$(this.selector_doc_field_show_all).is(':checked') ? 'inline-block' : 'none');
        $(this.class_doc_field_show_all+'.check-box-label-on').css('display',$(this.selector_doc_field_show_all).is(':checked') ? 'none' : 'inline-block');
    }

    checkAddExplain()
    {
        $(this.class_add_explain+'.check-box-label-off').css('display',$(this.selector_add_explain).is(':checked') ? 'inline-block' : 'none');
        $(this.class_add_explain+'.check-box-label-on').css('display',$(this.selector_add_explain).is(':checked') ? 'none' : 'inline-block');
    }

    setResultQueryListener()
    {
        $(this.selector_result_query).on('keyup',{ caller: this },function(e)
        {
            e.data.caller.toggleRunAlteredQuery();
        });
    }

    toggleRunAlteredQuery()
    {
        $(this.selector_run_altered_query).attr('disabled',($(this.selector_result_query).val()==this.result_query_url))
    }

    toggleAddExplain()
    {
        if (!this.service)
        {
            return;
        }

        if (!this.service.explainable)
        {
            $(this.selector_add_explain).prop('checked',false);
        }

        this.checkAddExplain();

        if (this.explainBlinked)
        {
            $(this.class_explain_group).removeClass('do-blink');
        }

        $(this.class_explain_group).toggle(this.service.explainable);

        if (!this.explainBlinked && this.service.explainable)
        {
            $(this.class_explain_group).addClass('do-blink');
            this.explainBlinked=true;
        }
    }

    enableCtrlSToggle()
    {
        $(this.class_enable_ctrl_save+'.check-box-label-off').css('display',$(this.selector_enable_ctrl_save).is(':checked') ? 'inline-block' : 'none');
        $(this.class_enable_ctrl_save+'.check-box-label-on').css('display',$(this.selector_enable_ctrl_save).is(':checked') ? 'none' : 'inline-block');
    }

    enableMonthlyBackupToggle()
    {
        $(this.class_enable_backup+'.check-box-label-off').css('display',$(this.selector_enable_backup).is(':checked') ? 'inline-block' : 'none');
        $(this.class_enable_backup+'.check-box-label-on').css('display',$(this.selector_enable_backup).is(':checked') ? 'none' : 'inline-block');
    }

    toggleHelp(state)
    {
        $(this.selector_help).toggle(state);
    }

    clearResultSearch()
    {
        $(this.selector_result_search).val('');
    }

    checkExportQueryToggle( number )
    {
        $(this.selector_prefix_export+number+'-on').css('display',$(this.selector_prefix_export+number).is(':checked') ? 'inline-block' : 'none');
        $(this.selector_prefix_export+number+'-off').css('display',$(this.selector_prefix_export+number).is(':checked') ? 'none' : 'inline-block');
    }

    checkExportGroupToggle( groupid )
    {
        var caller = this;

        $(this.selector_prefix_group_export+groupid+'-on').css('display',$(this.selector_prefix_group_export+groupid).is(':checked') ? 'inline-block' : 'none');
        $(this.selector_prefix_group_export+groupid+'-off').css('display',$(this.selector_prefix_group_export+groupid).is(':checked') ? 'none' : 'inline-block');

        $(this.selector_prefix_group+groupid+' li').each(function()
        {
            $(this).find(caller.class_query_export).first().prop('checked',$(caller.selector_prefix_group_export+groupid).is(':checked')).trigger('change');
        },
        caller);
    }

    setWaitCursor( state )
    {
        if (state)
        {
            $('body').css('cursor','wait');
        }
        else
        {
            $('body').css('cursor','default');
        }
    }

    makeClickableJsonParseError(err)
    {
        var patt = /(position) ((\d)+)/i;
        return err.replace(patt,this.getTemplate("clickableJsonError_tpl"));
    }

    toggleQuickExportButton(state)
    {
        $(this.class_quick_export).toggle(state);
    }

    fileInputEventBind()
    {
        $(this.selector_file_input).on('change',function(e)
        {
            if (!stored_queries.getIsLoadingBackup())
            {
                stored_queries.setIsLoadingBackup(true);
                main.readUploadFile( e );
            }
        });
    }

    toggleResultFullScreen(state)
    {
        if (state)
        {
            $(this.selector_local_results).attr("title",this.title_results_full_screen);
            $(this.selector_local_results).addClass("full-screen-results");
        }
        else
        {
            $(this.selector_local_results).attr("title",this.title_results_normal);
            $(this.selector_local_results).removeClass("full-screen-results");

        }

    }
}