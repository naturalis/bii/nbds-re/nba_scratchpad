class JsonPath extends Base {

	selector_json_path = '#JSONpath'
	selector_json_path_dialog = '#JSONpathDialog'
	selector_json_path_dialog_content = '#JSONpathDialogContent'
	class_drag_handle = '.JSONpathDialogDragHandle'
	class_example = '.JSONpath_example'

	initial_path = '$.resultSet[*].item'

	chosenPath = ""
	JSONexplanation = ""

	setLatestResults(latest_results)
	{
		this.latest_results = latest_results;
	}

	clearPathResults()
	{
		$(this.selector_json_path_dialog_content).html(this.getTemplate("helpText_JSONpath"));
		$(this.class_example).remove('dblclick').on('dblclick',function() { $(this.selector_json_path).val($(this).html()); });
	}

	clearInput()
	{
		$(this.selector_json_path).val("");
	}

	init()
	{
		$(this.selector_json_path_dialog).position({
		   my: "center",
		   at: "center",
		   of: window
		});

		$(this.selector_json_path_dialog).draggable({ handle: this.class_drag_handle });
		$(this.class_example).remove('dblclick').on('dblclick',function() { $(this.selector_json_path).val($(this).html()); });
	}

	findPath()
	{
		this.chosenPath = $(this.selector_json_path).val();

		if (this.chosenPath.length==0)
		{
			this.clearPathResults();
			return;
		}

		$(this.selector_json_path_dialog_content).css("color","#000");

		var out = jsonPath(this.latest_results,this.chosenPath);

		if (out==false)
		{
			var n = this.chosenPath.lastIndexOf(".");
			var newChosenPath = (this.chosenPath.substr(0,n) + "[*]" + this.chosenPath.substr(n)).replace("[*][*]","[*]").replace("..",".");
			out = jsonPath(this.latest_results,newChosenPath);
			if (out)
			{
				$(this.selector_json_path).val(newChosenPath);
			}
		}

		if (out)
		{
			$(this.selector_json_path_dialog_content).html(JSON.stringify(out,null,4));
			return true;
		}
		else
		{
			$(this.selector_json_path_dialog_content).css("color","#a00");
			setTimeout(function() { $(this.selector_json_path_dialog_content).css("color","#000"); }, 250);
			return false;
		}
	}

	copyResultToClipboard( includeQuery )
	{
		functions.copyToClipboard(
			( includeQuery ? this.chosenPath + "\n" : "" ) +
			$(this.selector_json_path_dialog_content).html()
		);
	}

	addElementToPath()
	{
		if (!window.x)
		{
			var x = {};
		}

		x.Selector = {};

		x.Selector.getSelected = function()
		{
			var t = '';
			if (window.getSelection)
			{
				t = window.getSelection();
			}
			else
			if (document.getSelection)
			{
				t = document.getSelection();
			} else
			if (document.selection)
			{
				t = document.selection.createRange().text;
			}
			return t;
		}

		var mytext = x.Selector.getSelected();
		var curr=$(this.selector_json_path).val();

		$(this.selector_json_path).val(curr + "." + mytext);

		if (!this.findPath())
		{
			$(this.selector_json_path).val(curr);
			this.findPath();
		}
	}

	pathDblClick()
	{
		var curr=$(this.selector_json_path).val();

		if (curr.length==0)
		{
			$(this.selector_json_path).val(this.initial_path);
		}
		else
		{
			$(this.selector_json_path).val( curr.substr(0,curr.lastIndexOf(".") ) );
		}

		this.findPath();
	}

}