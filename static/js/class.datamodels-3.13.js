class DataModels extends Base
{
	nonIndexedFields=[]
	availableFields=[]
	dynamicOperatorList=[]
	documentTypes=[]

	constructor(config,server)
	{
		super(config);
		this.setServer(server);
	}

	getAvailableFields()
	{
		return this.availableFields;
	}

	setDocumentTypes(documentTypes)
	{
		this.documentTypes=documentTypes;
	}

	setDocumentType(documentType)
	{
		this.documentType = documentType;
	}

	setRawPathData(rawPathData)
	{
		this.rawPathData = rawPathData;
	}

	setRawFieldData(rawFieldData)
	{
		this.rawFieldData = rawFieldData;
	}

	setDataModels()
	{
		this.availableFields.length=0;
		this.nonIndexedFields.length=0;
		this.dynamicOperatorList.length=0;

		Array.from(this.documentTypes).forEach(docType =>
		{
			this.setDocumentType(docType);

			var p={
				async: false,
				dataType: "json",
				method: "POST",
				url: this.server.proxyUrl + this.server.proxyPath,
				data: {
					method: "GET",
					service: "/"+[this.config.nbaServerConfig.version, docType, this.config.nbaServerConfig.metaServiceUrls.getPaths].join("/")
				},
				caller: this,
				error: function(request, status, error)
				{
					//
				},
				success: function(data)
				{ 
					this.caller.setRawPathData(data);
					this.caller.processPathData();
				},
				complete: function()
				{
					// 
				}
			};

			$.ajax( p );

			var p={
				async: false,
				dataType: "json",
				method: "POST",
				url: this.server.proxyUrl + this.server.proxyPath,
				data: {
					method: "GET",
					service: "/"+[this.config.nbaServerConfig.version, docType, this.config.nbaServerConfig.metaServiceUrls.getFieldInfo].join("/")
				},
				caller: this,
				error: function(request, status, error)
				{
					// 
				},
				success: function(data)
				{ 
					this.caller.setRawFieldData(data);
					this.caller.processFieldData();
				},
				complete: function()
				{
					// 
				}
			};

			$.ajax( p );

		});
		
		this.setMessage( "done reading datamodels" );

		if (this.dynamicOperatorList.length>0)
		{
			this.dynamicOperatorList = this.dynamicOperatorList.filter(Boolean);
			this.config.queryOperators.length=0
			this.config.queryOperators=this.dynamicOperatorList;
			this.setMessage( "loaded dynamic operator list" );
		}
		else
		{
			this.setMessage( "loaded default operator list (couldn't read dynamic)" );
		}		
	}

	processPathData()
	{
		if (this.rawPathData==null) 
		{
			this.setMessage( "didn't find datamodels for "+this.documentType );
			return;
		}
		
		Array.from(this.rawPathData).forEach(f=>
		{
			this.availableFields.push({ 
				document: this.documentType,
				label: this.documentType+' '+f,
				searchable: this.documentType+' '+f, 
				field: f 
			});				
		});
		
		this.setMessage( "read datamodels for "+this.documentType );
	}

	processFieldData()
	{
		function onlyUnique(value, index, self)
		{
			return self.indexOf(value) === index;
		}		

		if (this.rawFieldData==null) 
		{
			this.setMessage( "didn't find field info for "+this.documentType );
			return;
		}

		for(var field in this.rawFieldData)
		{
			var f=this.rawFieldData[field];

			for(var i=0;i<this.availableFields.length;i++)
			{
				if (this.availableFields[i].document==this.documentType && this.availableFields[i].field==field)
				{
					this.availableFields[i].type=f.type;
					this.availableFields[i].indexed=(f.indexed===true || f.indexed=="true");
					this.availableFields[i].allowedOperators=f.allowedOperators;
				}
			}

			this.dynamicOperatorList=this.dynamicOperatorList.concat( f.allowedOperators ).filter(onlyUnique);

			if (!f.indexed)
			{
				this.nonIndexedFields.push({ 
					document: this.documentType,
					field: field,
					label: this.documentType+' '+field
				});				
			}
		}
		this.setMessage( "read field info for "+this.documentType );
	}
}