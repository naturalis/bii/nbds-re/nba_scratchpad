class Scratchpad extends Base
{

    server_selector = "#server"
    version_info_selector = '#version-history li'
    server = {}
    fatalErrors = []
    applicationInfo = null;
    url = null

    constructor(config,applicationInfo)
    {
        super(config);
        this.applicationInfo = applicationInfo;
        this.url = new URL(window.location.href);
    }

    checkMinimalRequirements()
    {
        if ("undefined" === typeof this.config.nbaServerConfig)
        {
            this.fatalErrors.push("NBA server settings are missing");
            return false;
        }
        return true;
    }

    getFatalErrors()
    {
        return this.fatalErrors;
    }

    printApplicationInfo()
    {
        this.version_info = $(this.version_info_selector).first().html().trim().split(":").shift().split(" ",2).map(function(e,i){ return e.replace(/(\(|\)|\s)/gi,""); });

        $("title").html(this.applicationInfo.name);
        $(".appName").html(this.applicationInfo.name);
        $(".appVersion").html(this.version_info[0] ? this.version_info[0] : this.applicationInfo.version);
        $(".appDate").html(this.version_info[1] ? this.version_info[1] : this.applicationInfo.date);
        $(".appAuthor").html(this.applicationInfo.author);
        $(".appEmail").attr("href","mailto:"+this.applicationInfo.email);
    }

    setServer( p )
    {
        p.url.replace(/\/$/,"");
        p.label = p.url;
        p.testpath = p.nbaServer + "/" + p.nbaVersion + "/";
        p.noServices = false;
        p.allowCrossDomain = true;
        p.directNbaPath=p.nbaServer + "/" + p.nbaVersion + "/";
        this.server=p;
        $(this.server_selector).html( p.url );
    }

    getServer()
    {
        return this.server;
    }

}