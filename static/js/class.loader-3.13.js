class Loader extends Base
{
	constructor(config)
	{
		super();
	}

	disablePage()
	{
		$('#loader').toggle( true );
		$('.loader').toggle( true );
		this.toggleElementsDisabled( true );
	}

	toggleElementsDisabled( state )
	{
		$('input,textarea,select').each(function(ele,index)
		{
			if ($(this).attr('data-disabled')==1 && state==false)
			{
				$(this).prop('disabled',true);
			}
			else
			{
				$(this).prop('disabled',state);
			}
		});
	}

	enablePage( msg='done loading' )
	{
		this.toggleElementsDisabled( false );
		$('#loader').fadeOut(2000);
		$('.loader').fadeOut(2000);
		this.setMessage(msg,5000);
	}

}