class Results extends Base {

	selector_localresults = '#localresults'
	selector_result_search_count = '#resultSearchCount'
	selector_result_search_line = '#resultSearchLine'
	selector_save_result_filename = '#saveResultFilename'
	selector_save_result_filename_counter = '#saveResultFilenameCounter'
	selector_result_search = '#resultSearch'

	fullRequestUrl = null
	contentType = null
	data = null
	server = null
	log = null
	latestResult = { data: null, contentType: null, formatted: null, tsv: null, log: null }
	latestResultSize = 0
	latestTotalResultSize = 0
	prevFound = -1
	prevPrevFound = 0
	resultView = null

	prevFilename =  null
	defaultFilename = "nba-query-results"
	useCounter =  false
	fileCounter =  0

	requestError = { err: null, message: null, url: null }

	maxDisplayedSize = 1000000 // bytes
	maxDisplayRoundTo = 10
	docsDisplayed = null
	isDisplayedCapped = false

	googleMapsUrl = 'https://www.google.com/maps/place/%LAT%,%LONG%/';

    constructor(config)
    {
        super(config);
    }

	clear()
	{
		$(this.selector_localresults).html("");
		$('.result-total-size').html("0");
	}

	setLatestResult(latestResult)
	{
		this.latestResult = latestResult;
	}

	getLatestResult()
	{
		return {
			...this.latestResult,
			...{stringified: this.getStringified(),docsDisplayed: this.docsDisplayed,isDisplayedCapped: this.isDisplayedCapped}
		};
	}

	setLatestResultProperty(property)
	{
    	this.latestResult = {...this.latestResult, ...property};
	}

	resetLatestResult()
	{
		this.latestResult = { data: null, contentType: null, formatted: null, tsv: null, log: null };
	}

	setFullRequestUrl(fullRequestUrl)
	{
		this.fullRequestUrl = fullRequestUrl;
	}

	setContentType(contentType)
	{
		this.contentType = contentType;
	}

	setServer(server)
	{
		this.server = server;
	}

	setCurrentResultView(resultView)
	{
		this.resultView = resultView;
	}

	getStringified(indentation='\t',data)
	{
		// return JSON.stringify(data ?? this.latestResult.data, undefined, indentation);
		return JSON.stringify(data ?? this.latestResult.display_data, undefined, indentation);
	}

	setFormatted()
	{
		this.latestResult.formatted = this.makeFormatted(this.getStringified());
	}

	getDocsDisplayed()
	{
		return this.docsDisplayed;
	}

	setLatestResultSize(latestResultSize)
	{
		this.latestResultSize = latestResultSize;
	}

	getLatestResultSize()
	{
		return this.latestResultSize;
	}

	setLatestTotalResultSize(latestTotalResultSize)
	{
		this.latestTotalResultSize = latestTotalResultSize;
	}

	getLatestTotalResultSize()
	{
		return this.latestTotalResultSize;
	}

	setLog(log)
	{
		this.log = log;
	}

	resetDisplayData()
	{
		this.docsDisplayed = null;
		this.latestResult.display_data = null;
		this.isDisplayedCapped = false;
	}

	applyResultDisplayCap()
	{
		this.latestResult.display_data = functions.deepCopy(this.latestResult.data);

		if (!this.latestResult.data || !this.latestResult.data.resultSet)
		{
			return;
		}

		var num_of_docs = this.latestResult.data.resultSet.length;
		var char_size = this.getStringified('\t',this.latestResult.data).length;
		var avg_per_doc = (char_size / num_of_docs);
		this.docsDisplayed = Math.ceil(this.maxDisplayedSize / avg_per_doc);
		this.docsDisplayed = this.docsDisplayed < this.maxDisplayRoundTo ?
			this.maxDisplayRoundTo :
			(this.docsDisplayed - (this.docsDisplayed % this.maxDisplayRoundTo));

		this.latestResult.display_data.resultSet = this.latestResult.data.resultSet.slice(0,this.docsDisplayed);
		this.docsDisplayed = this.latestResult.display_data.resultSet.length;
		this.isDisplayedCapped = this.latestResult.display_data.resultSet < this.latestResult.data.resultSet;

		if (this.isDisplayedCapped)
		{
			this.latestResult.display_data.note =
				this.getTemplate("maxDisplayedMessageTpl")
					.replace("<br />"," ")
					.replace("%CAP%",this.docsDisplayed)
					.replace("%ALL%",this.latestResult.data.resultSet.length);
		}
	}

	makeFormatted(content)
	{
		var buffer = [];

		Array.from(functions.syntaxHighlight(content).split("\n")).forEach(function(arg,i)
		{
			if (arg.indexOf('<span')>0)
			{
				arg = arg.replace(`<span`,`<span id="rl${i}"`);
			}

			if (arg.toLowerCase().indexOf('https://')>0 || arg.toLowerCase().indexOf('http://')>0)
			{
				arg = arg.replace(/"((https|http):\/\/[^"]*)"/i,"\"<a href=\"$1\" target=\"_blank\">$1</a>\"");
			}

			if (arg.indexOf('longitudeDecimal')>0)
			{
				arg = arg.replace('longitudeDecimal',`<span class="site-coordinates" data-line-id="${i}">longitudeDecimal</span>`);
			}
			else
			if (arg.indexOf('latitudeDecimal')>0)
			{
				arg = arg.replace('latitudeDecimal',`<span class="site-coordinates" data-line-id="${i}">latitudeDecimal</span>`);
			}

			buffer.push(arg);
		});

		return buffer.join("\n");
	}

	printResults()
	{
		$('.resultUrl').attr("href",this.fullRequestUrl);
		$('.results').toggle(true);

		if ( this.contentType == "text/html" )
		{
			$( "#remoteresults" ).attr( "data", this.fullRequestUrl );
		}
		else
		if ( this.contentType == "text/plain" && !this.hasExplainLog() && this.latestResult.data.indexOf("Exception")>-1)
		{
			var err = this.latestResult.data.split("\n")[0];
			var exc = err.split(/:/)[0].trim();
			err = err.replace(exc,'').trim();

			this.latestResult.data = {
				"nba_error": {
					"error": err,
					"requestUri": this.fullRequestUrl
						.replace(this.server.private_nba_address,this.server.public_nba_address),
					"exception": exc
				}
			}

			this.latestResult.display_data = functions.deepCopy(this.latestResult.data);
			this.setFormatted();
			this.setMessage( this.getTemplate( "nbaErrorMsgTpl" ).replace( "%ERROR%",exc), false );
			var element = document.getElementById(this.selector_localresults.substring(1));
			element.innerHTML = this.getTemplate("responseSuccessTpl").replace('%RESPONSE%',this.latestResult.formatted);
		}
		else
		if ( this.contentType == "text/plain" && !this.hasExplainLog() )
		{
			$( this.selector_localresults ).html(
				this.getTemplate( "responseSuccessTpl" )
					.replace( "%RESPONSE%", this.latestResult.data ) );
		}
		else
		if ( this.contentType != "application/json" && !this.hasExplainLog() )
		{
			$( this.selector_localresults ).html(
				this.getTemplate( "otherFormatsMessageTpl" )
					.replace( "%CONTENT_TYPE%", this.contentType )
					.replace( "%URL%", this.fullRequestUrl )
			);
		}
		else
		{
			if (this.latestResult.data.scratchpad_proxy_error)
			{
				this.setMessage( this.getTemplate( "nbaErrorMsgTpl" )
					.replace( "%ERROR%", this.latestResult.data.scratchpad_proxy_error.error), false );
			}
			else
			if (this.latestResult.data.exception)
			{
				var err = `${this.latestResult.data.httpStatus.code} (${this.latestResult.data.httpStatus.message})`;

				if (this.latestResult.data.exception.message)
				{
					var exc = this.latestResult.data.exception.message.split("\n")[0].replaceAll('"',"'")
				}
				else
				{
					var exc = this.latestResult.data.exception.type;
				}

				this.latestResult.data = {
				    "nba_error": {
				        "error": err,
				        "requestUri": this.latestResult.data.requestUri
				        	.replace(this.server.private_nba_address,this.server.public_nba_address),
				        "exception": exc
				    }
				}

				this.latestResult.display_data = functions.deepCopy(this.latestResult.data);

				this.setFormatted();
				this.setMessage( this.getTemplate( "nbaErrorMsgTpl" ).replace( "%ERROR%",err), false );
			}

			if (this.hasExplainLog())
			{
				this.setSplitOffExplainLog();
				this.contentType = "application/json";
			}

			this.timer.checkpoint( this.timer.QUERY_EXECUTION, "formatted response" );

			// avoiding further jQuery overhead
			var element = document.getElementById(this.selector_localresults.substring(1));
			element.innerHTML = this.getTemplate("responseSuccessTpl").replace('%RESPONSE%',this.latestResult.formatted);

			this.siteCoordinatesBindOnClick();
			this.docKeysBindOnClick();
			this.showExplainLog();
		}

		this.setResultRecordCount();
		this.printResultRecordCount();
	}

	siteCoordinatesBindOnClick()
	{
		$('.site-coordinates').on('click',{ caller: this },function(e)
		{
			e.data.caller.openSiteCoordinates(parseInt($(this).attr('data-line-id')));
		});
	}

	openSiteCoordinates(line)
	{
		var longitudeDecimal;
		var latitudeDecimal;

		Array.from(this.getStringified().split("\n")).forEach(function(arg,i)
		{
			if (i>=(line-1) && i<=(line+1))
			{
				if (arg.indexOf('longitudeDecimal')>0)
				{
					longitudeDecimal = arg;
				}
				else
				if (arg.indexOf('latitudeDecimal')>0)
				{
					latitudeDecimal = arg;
				}
			}
		});

		const regex = /[A-Z:"\s,]/ig;
		latitudeDecimal = Number(latitudeDecimal.replace(regex,''));
		longitudeDecimal = Number(longitudeDecimal.replace(regex,''));

		// console.log(latitudeDecimal,longitudeDecimal);

		var url = this.googleMapsUrl
			.replace('%LAT%',latitudeDecimal ?? 0)
			.replace('%LONG%',longitudeDecimal ?? 0);

		window.open(url,"_blank");
	}

	docKeysBindOnClick()
	{
		$('span.key').on('dblclick',{ caller: this },function(e)
		{
			if (bindKeys.getCtrlPressed())
			{
				e.data.caller.copyDocKeyVal($(this).attr('id').replace('rl',''));
			}
		});
	}

	copyDocKeyVal(start_line)
	{
		var brackets = 0;
		var buffer = [];

		Array.from(this.getStringified().split("\n")).forEach(function(arg,i)
		{
			var sanitized = arg
				.replaceAll('\{','')
				.replaceAll('\}','')
				.replaceAll('\[','')
				.replaceAll('\]','');

			if (start_line==i)
			{
				buffer.push(arg);
				brackets = ((arg.indexOf('{')>0)  || (arg.indexOf('[')>0) ? 1 : 0);
				// console.log(i,arg,x);
			}
			else
			if (brackets>0)
			{
				buffer.push(arg);
				brackets = brackets + ((arg.indexOf('{')>0) ? 1 : 0);
				brackets = brackets + ((arg.indexOf('[')>0) ? 1 : 0);
				brackets = brackets - ((arg.indexOf('}')>0) ? 1 : 0);
				brackets = brackets - ((arg.indexOf(']')>0) ? 1 : 0);
			}
		});

		const regex = /\t/ig;
		buffer = buffer.map(function(a){ a=a.replaceAll(regex,""); return a;});
		var snippet = "{" + buffer.join("\n").replace(/,(\s*)$/g, '') + "}";
		snippet = JSON.parse(snippet);

		if (functions.copyToClipboard(JSON.stringify(snippet)))
		{
			this.setMessage('copied key/value to clipboard');
		}
		else
		{
			this.setMessage('result too large for automated copying');
		}
	}

	hasExplainLog()
	{
		try
		{
			return this.latestResult.data.indexOf(this.config.explainLogBoundary)>-1;
		} catch(err) {
			return false;
		}
	}

	setSplitOffExplainLog()
	{
		var t = this.latestResult.data.split(this.config.explainLogBoundary);
		this.latestResult.data = JSON.parse(t[1].trim());
		this.setFormatted();
		this.setLog(t[0].trim());
	}

	showExplainLog()
	{
		if (this.log)
		{
			functions.dialog(
				"query explain log output",
				this.getTemplate('preTpl').replace( "%CONTENT%",this.log ),
				null,
				null,
				{ resizable: true, draggable: true, buttons: { ok() { $( this ).dialog( "close" ); } } }
			);

			$('.show-explain-log').toggle(true);
		}
		else
		{
			$('.show-explain-log').toggle(false);
		}
	}

	setResultRecordCount()
	{
		this.setLatestResultSize(0);
		this.setLatestTotalResultSize(0);

		if(!this.latestResult.data)
		{
			return;
		}

		if(this.latestResult.data.resultSet)
		{
			this.setLatestResultSize(this.latestResult.data.resultSet.length);

			if(this.latestResult.data.totalSize)
			{
				this.setLatestTotalResultSize(this.latestResult.data.totalSize);
			}
			else
			{
				this.setLatestTotalResultSize(this.latestResult.data.resultSet.length);
			}
		}
		else
		{
			if (Array.isArray(this.latestResult.data))
			{
				var keys = Object.keys(this.latestResult.data);
				this.setLatestResultSize(keys.length);
				this.setLatestTotalResultSize(keys.length);
			}
			else
			{
				try
				{
					this.setLatestResultSize(Object.keys(this.latestResult.data).length);
					this.setLatestTotalResultSize(Object.keys(this.latestResult.data).length);
				} catch(err) {
					this.setLatestResultSize(0);
					this.setLatestTotalResultSize(0);
				}
			}
		}
	}

	printResultRecordCount()
	{
		if(this.getDocsDisplayed())
		{
			$( ".result-total-size" ).html(
				this.getTemplate("totCountTpl")
					.replace('%COUNT%',Intl.NumberFormat().format(this.getDocsDisplayed()))
					.replace('%TOTAL%',Intl.NumberFormat().format(this.latestTotalResultSize))
			);
		}
		else
		if (this.latestResultSize==this.latestTotalResultSize)
		{
			$( ".result-total-size" ).html(Intl.NumberFormat().format(this.latestResultSize));
		}
		else
		{
			$( ".result-total-size" ).html(
				this.getTemplate("totCountTpl")
					.replace('%COUNT%',Intl.NumberFormat().format(this.latestResultSize))
					.replace('%TOTAL%',Intl.NumberFormat().format(this.latestTotalResultSize))
			);
		}
	}

	findAndScroll( next )
	{
		if (next=='up')
		{
			this.prevFound = this.prevPrevFound-1;
		}
		else
		if (next!='down')
		{
			this.prevFound=0;
		}

		var s=$(this.selector_result_search).val().toLowerCase();
		var found=0;

		$(this.selector_localresults + ' span').removeClass('pale-yellow-highlight');

		if (s.length==0)
		{
			$(this.selector_localresults).scrollTop(0);
		}
		else
		{
			var lines = this.latestResult.formatted.split('\n');
			var first = true;

			Array.from(lines).forEach(function(line,i)
			{
				if (functions.stripTags(line).toLowerCase().indexOf(s)>0)
				{
					if (i>this.prevFound && first)
					{
						$(this.selector_localresults).scrollTo( $(`#rl${i}`), 100, {axis: 'y'} );

						$(`#rl${i}`).addClass('pale-yellow-highlight').next('span').addClass('pale-yellow-highlight');

						this.prevPrevFound = this.prevFound;
						this.prevFound = i;
						first=false;
					}
					found++;
				}
			},this);
		}

		$(this.selector_result_search_count).html(found);
		$(this.selector_result_search_line).html(this.prevFound);
	}

	resetFileCounter()
	{
		var prev = this.fileCounter;
		this.fileCounter = 0;
		this.setMessage(`reset counter from ${prev} to ${this.fileCounter}`);
	}

	resetResultFileName()
	{
		$(this.selector_save_result_filename).val(this.defaultFilename);
	}

	downloadResults(fromDialog=false)
	{
		var filename = this.prevFilename ?? this.defaultFilename;

		if (fromDialog)
		{
			var filename = $(this.selector_save_result_filename).val();
			this.useCounter = $(this.selector_save_result_filename_counter).prop('checked');
		}
		else
		if (bindKeys.getCtrlPressed())
		{
			functions.dialog("download results",this.getTemplate('saveResultDialogTpl'),main.test);
			$(this.selector_save_result_filename).val(filename)
			$(this.selector_save_result_filename_counter).prop('checked',this.useCounter);
			return;
		}

		if (this.resultView==this.VIEW_TYPE_TABULAR)
		{
			var ext = "tsv";
			var content = this.latestResult.tsv;
		}
		else
		{
			if (this.latestResult.contentType=="application/json")
			{
				var ext = "json";
				var content = JSON.stringify(this.latestResult.data, undefined, 4);
			}
			else
			{
				var ext = "txt";
				var content = this.latestResult.raw;
			}
		}

		filename = filename.replace(/\.json$/i,'');
		filename = filename.length==0 ? this.defaultFilename : filename;
		this.prevFilename = filename;
		filename = filename + (this.useCounter ? `-${functions.leftPad(this.fileCounter++,3,"0")}` : "");
		filename = `${filename}--(${functions.getDateString(true,true)}).${ext}`;
		functions.createLocalDownloadFile(content,filename);
	}

	setRequestError(error, message, url)
	{
		this.requestError = { error: error, message: message, url: url };
	}

	showRequestError()
	{
		var a = {
			"scratchpad_proxy_error" : {
				"error" : this.requestError.error,
				"nba_url" : this.requestError.url,
				"message" : this.requestError.message,
			}
		}

		$( this.selector_localresults ).html(
			this.getTemplate("responseSuccessTpl")
				.replace('%RESPONSE%',this.makeFormatted(JSON.stringify(a, undefined, 4))
		));
	}

	copyResultsToClipboard(type="json")
	{
		if (type=="tsv")
		{
			// var r = this.getLatestResult().tsv;

			var all = Array();

			$("table.tabular-data tr").each(function()
			{
			    var rows = Array();
				$(this).find("td,th").each(function()
				{
					var spans = $(this).find("span");
					rows.push($(spans[0]).text().trim());
				})

			    all.push(rows.join("\t"));
			})

			var r = all.join("\n");
		}
		else
		if (bindKeys.getCtrlPressed())
		{
			var r = this.getTemplate('jiraNoFormatTpl').replace('%QUERY%', this.getStringified())
		}
		else
		{
			var r = this.getStringified();
		}

		if ( functions.copyToClipboard(r) )
		{
			this.setMessage('copied result to clipboard');
		}
		else
		{
			this.setMessage('result too large for automated copying');
		}
	}

	copyResultURLToClipboard()
	{
		if (bindKeys.getCtrlPressed())
		{
			var r = this.getTemplate('jiraNoFormatTpl').replace('%QUERY%', $('#resultQuery').val());
		}
		else
		{
			var r = $('#resultQuery').val();
		}

		if ( functions.copyToClipboard(r) )
		{
			this.setMessage('copied result to clipboard');
		}
		else
		{
			this.setMessage('result too large for automated copying');
		}
	}

	copyScratchpadURLToClipboard(query_text,service_key)
	{
		var p=[];

		p.push(`q=${functions.rawurlencode(query_text)}`);
		p.push(`k=${functions.rawurlencode(service_key)}`);
		p.push(`e=${(bindKeys.getCtrlPressed()?'1':'0')}`);
		p.push(`s=${(bindKeys.getAltGrPressed()?'0':'1')}`);

		console.log(`${scratchpad.url.origin}${scratchpad.url.pathname}?${p.join("&")}`);
		if ( functions.copyToClipboard(`${scratchpad.url.origin}${scratchpad.url.pathname}?${p.join("&")}`) )
		{
			this.setMessage('copied Scratchpad URL to clipboard');
		}
		else
		{
			this.setMessage('URL too large for automated copying');
		}
	}

}