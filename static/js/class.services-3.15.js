class Services extends Base
{
	selector_services = "#services"

	service=null
	services=[]
	serviceIndex=[]
	serviceList=[]
	usingDefaultRestServiceLists=false
	testedServices=[]
	documentTypes=[]
	testInferredOnly=true
	serviceCounter = 0

	services_system = [ "ping", "release-notes", "reference-doc", "import-files" ]
	services_explainable = [ "queryWithNameResolution" ]
	services_cloneable = [ "query", "count", "download" ]
	services_accept_query_body = [ "query", "batchQuery", "count", "groupByScientificName", "queryWithNameResolution", "download", "dwca" ]
	services_query_body_mandatory = [ "query", "queryWithNameResolution" ]

	constructor(config,server)
	{
		super(config);
		this.setServer(server);
	}

	setService()
	{
		var selected = $(this.selector_services).val();

		this.service=Array.from(this.services).filter(a=>
		{
			if (selected)
			{
				return a.id == selected;
			}
			else
			{
				return a.default==true;
			}

		},selected).pop();
	}

	getService()
	{
		return this.service;
	}

	setServices()
	{
		this.services=[];
		this.serviceIndex=[];

		var p={
			async: false,
			dataType: "json",
			method: "POST",
			url: this.server.proxyUrl + this.server.proxyPath,
			data: {
				method: "GET",
				service: "/" + [this.config.nbaServerConfig.version,this.config.nbaServerConfig.metaServiceUrls.restServices].join("/")
			},
			caller: this,
			error: function (request, status, error)
			{
				this.caller.setMessage( "unable to retrieve services ("+error+")", false );
				this.caller.setDefaultRestServiceLists();
				this.caller.usingDefaultRestServiceLists=true;
				this.caller.processServiceList();
				this.caller.setMessage( "read all services (from default service list)" );
			},
			success: function(data)
			{
				if (data)
				{
					this.caller.setMessage( "read services" );
					this.caller.serviceList=data;
					this.caller.usingDefaultRestServiceLists=false;
					this.caller.processServiceList();
				}
				else
				{
					this.caller.setMessage( "unable to read services", false );
					this.caller.setDefaultRestServiceLists();
					this.caller.usingDefaultRestServiceLists=true;
					this.caller.processServiceList();
					this.caller.setMessage( "read all services (from default service list)" );
				}

				this.caller.sortServices();
				this.caller.getReferenceDocs();
			},
			complete: function()
			{
				// console.log(services);
			}
		};

		this.setMessage( "fetching service list" );

		$.ajax( p );
	}

	getServices()
	{
		return this.services;
	}

	getDocumentTypes()
	{
		return this.documentTypes;
	}

	setDefaultRestServiceLists()
	{
	    try {
	        this.serviceList = $.parseJSON( this.config.defaultServiceList );
	    }
	    catch (error) {
	        this.setMessage("cannot load defaultServiceList (error parsing JSON)");
	        this.serviceList = [];
	    }
	}

	processServiceList()
	{
		this.inferAbstractedServices();

		Array.from(this.serviceList).forEach(arg =>
		{
			var f = this.parseService( arg );

			if ( f )
			{
				if (this.serviceIndex.indexOf(f.key)==-1)
				{
					var clone = f.canClone;
					delete f.canClone;
					this.services.push( f );
					this.serviceIndex.push( f.key );
				}
				else
				{
					for(var j=0;j<this.services.length;j++)
					{
						if (this.services[j].key==f.key)
						{
							this.services[j].method=functions.arrayUnique(this.services[j].method.concat( f.method ));
							this.services[j].consumes = functions.arrayUnique((this.services[j].consumes ?? []).concat(f.consumes ?? []));
							this.services[j].produces = functions.arrayUnique((this.services[j].produces ?? []).concat(f.produces ?? []));
						}
					}
				}

				if (clone)
				{
					// clone object and make it into a 'human readable' service
					var copy = jQuery.extend({}, f);

					// copy.path = copy.path.replace("_querySpec=","");
					copy.label = copy.label.replace("_querySpec=","");
					copy.key=copy.label.trim().hashCode();
					copy.sort=copy.sort+"x";
					copy.default=false;

					copy.acceptsQueryString=copy.acceptsQueryBody;
					copy.acceptsQueryBody=false;
					copy.mandatoryQueryString=copy.mandatoryQueryBody;
					copy.mandatoryQueryBody=false;
					copy.id=this.serviceCounter++;

					copy.noQuery = (copy.pathParams.length==0 && !copy.acceptsQueryString && !copy.acceptsQueryBody); // convenience

					if (this.serviceIndex.indexOf(copy.key)==-1)
					{
						this.services.push( copy );
						this.serviceIndex.push( copy.key );
					}
				}
			}
			else
			{
				// console.log(arg);
			}
		});
	}

	inferAbstractedServices()
	{
		var newlist=[];

		Array.from(this.serviceList).forEach(arg =>
		{
			if (arg.endPoint.search(/{type:\([^\)]*\)}/)>-1)
			{
				var matches = arg.endPoint.match(/{type:(\([^\)]*\))}/);
				var types = matches[1].replace("(","").replace(")","").split("|")

				Array.from(types).forEach(type =>
				{
					let p1 = {
					    ...arg
					};

					p1.endPoint = arg.endPoint.replace(/{type:\([^\)]*\)}/,type).replace(/\/$/,"");
					p1.url = arg.url.replace(/{type:\([^\)]*\)}/,type).replace(/\/$/,"");
					p1.inferred = true;
					newlist.push(p1);
				});
			}
			else
			{
				arg.inferred = false;
				newlist.push(arg);
			}
		})

		this.serviceList = newlist;
	}

	parseService( s )
	{
		if (s.consumes && s.consumes.indexOf('application/x-www-form-urlencoded')!=-1)
		{
			return;
		}

		if (s.endPoint=="/")
		{
			return;
		}

		var service = {};

		service.method = [ s.method ];
		service.inferred = s.inferred;
		service.endPoint = s.endPoint;
		if (s.consumes) service.consumes = s.consumes;
		if (s.produces) service.produces = s.produces;

		var parts = s.endPoint.split("/");
		parts.shift(); // first element always empty

		// document
		service.document = parts.shift();

		if (this.services_system.indexOf(service.document)!=-1)
		{
			service.service=service.document;
			service.document=".system";
		}
		else
		if (service.document=="metadata")
		{
			service.document="." + service.document;
		}

		if (service.document.substr(0,1)!="." && this.documentTypes.indexOf(service.document)==-1)
		{
			this.documentTypes.push(service.document);
		}


		// service
		if (!service.service) service.service = parts.shift();
		service.explainable = this.services_explainable.indexOf(service.service) > -1;
		// service.canClone = !this.services_complex_only.includes(service.service);
		service.canClone = this.services_cloneable.includes(service.service);
		// service.forceNewWindow = ["download","dwca"].includes(service.service);
		service.forceNewWindow =
			service.produces &&
				(
					service.produces.includes("application/zip") ||
					service.produces.includes("application/x-ndjson;charset=UTF-8")
				);

		// "path parameters" (.../{field}/)
		service.pathParams=[];
		Array.from(parts).forEach(arg =>
		{
			if (arg.match(/{(.*)}$/))
			{
				service.pathParams.push(arg);
			}
		});

		parts.reverse().splice(0,service.pathParams.length).reverse();

		// exception
		if (service.service=="dwca" && parts[0]== "query")
		{
			service.canClone = true;
		}

		// query body
		service.acceptsQueryBody = this.services_accept_query_body.includes(service.service) || service.service.match(/^getDistinct/) || service.service.match(/^countDistinct/);
		service.mandatoryQueryBody = service.acceptsQueryBody && this.services_query_body_mandatory.includes(service.service);

		// query string
		service.acceptsQueryString = false;
		service.mandatoryQueryString = false;
		service.noQuery = (service.pathParams.length==0 && !service.acceptsQueryString && !service.acceptsQueryBody); // convenience

		// service path (w/o {params})
		var p = ["",this.config.nbaServerConfig.version];
		if (service.document!=".system")
		{
			p.push(service.document.replace(/^\./,''));
		}
		p.push(service.service);
		service.path = p.concat(parts).join("/");

		service.sort = this.createSortValue(service);
		service.default = (service.sort=="a001"); //  && hasQuerySpec)

		// label etc
		service.label = "/" + this.config.nbaServerConfig.version + s.endPoint + (service.acceptsQueryBody ? "/?_querySpec=" : "");
		// service.label = "/" + this.config.nbaServerConfig.version + s.endPoint;
		service.key = this.makeHashCode(s,service);
		service.id = this.serviceCounter++;

		return service;
	}

	makeHashCode(s,service)
	{
		// legacy code (must match saved hashcodes)
		var pathParts=s.endPoint.trim().split("/");
		pathParts.shift();
		pathParts.shift();
		var lastPart=pathParts.pop();

		var path = "/"+this.config.nbaServerConfig.version+s.endPoint.trim().replace(/{(.*)}$/,'');

		if (this.services_accept_query_body.includes(lastPart) && service.document.substr(0,1)!=".")
		{
			path=path+"/?_querySpec=";
		}

		return path.trim().hashCode();
	}

	createSortValue(service)
	{
		var sort="";

		if (service.document=="specimen") sort="a";
		if (service.document=="taxon") sort="b";
		if (service.document=="multimedia") sort="c";
		if (service.document=="names") sort="d";
		if (service.document=="geo") sort="e";
		if (service.document==".metadata") sort="x";
		if (service.document==".system") sort="z";

		var f=[
			{ service: 'query' },
			{ service: 'queryWithNameResolution' },
			{ service: 'batchQuery' },
			{ service: 'getDistinctValues' },
			{ service: 'getDistinctValuesPerGroup' },
			{ service: 'countDistinctValues' },
			{ service: 'countDistinctValuesPerGroup' },
			{ service: 'getNamedCollections' },
			{ service: 'getIdsInCollection' },
			{ service: 'find' },
			{ service: 'exists' },
			{ service: 'count' },
			{ service: 'download/' },
		]

		var subsort=999;
		if (service.service == 'dwca')
		{
			subsort=90;
		}
		else
		if (service.service == 'metadata')
		{
			subsort=100;
		}
		else
		{
			Array.from(f).forEach(function (arg,index)
			{
				if (service.service == arg.service)
				{
					subsort=index+1;
				}
			});
		}

		return sort + functions.leftPad(subsort,3,"0");
	}

	sortServices()
	{
		this.services.sort(function(a,b)
		{
			if (a.sort < b.sort) return -1;
			if (a.sort > b.sort) return 1;

			if (a.path < b.path) return -1;
			if (a.path > b.path) return 1;

			return 0;
		});
	}

	getReferenceDocs()
	{
		var p={
			async: false,
			dataType: "json",
			method: "POST",
			url: this.server.proxyUrl + this.server.proxyPath,
			data: {
				method: "GET",
				service: "/"+[this.config.nbaServerConfig.version,this.config.nbaServerConfig.metaServiceUrls.referenceDoc].join("/")
			},
			caller: this,
			success: function(data)
			{
				this.caller.referenceDocData = data;
				this.caller.addReferenceDocData();
			},
			error: function(request, status, error)
			{
				// console.log( "error fetching reference docs: " + error );
				this.caller.setMessage( "error fetching reference docs: " + error);
			},
			complete: function()
			{
				//
			}
		};

		$.ajax( p );
	}

	addReferenceDocData()
	{
	    for (var arg in this.referenceDocData.paths)
	    {
	    	var summary = "";

	    	if (this.referenceDocData.paths[arg].get && this.referenceDocData.paths[arg].get.summary)
	    	{
	    		summary = this.referenceDocData.paths[arg].get.summary;
	    	}
	    	else
			if (this.referenceDocData.paths[arg].post && this.referenceDocData.paths[arg].post.summary)
	    	{
				summary = this.referenceDocData.paths[arg].post.summary;
	    	}

	    	for (var key in this.services)
	    	{
				if (this.services[key].endPoint == arg)
				{
					this.services[key].summary = summary;
				}
	    	}
	    }
	}

	addStaticServices()
	{
		if ("undefined" === typeof this.config.staticServices) return;
		this.services = this.services.concat(this.config.staticServices);
	}

	testServices()
	{
		var testpayload=[];

		Array.from(this.services).forEach(arg =>
		{
			if ((this.testInferredOnly && arg.inferred) || !this.testInferredOnly)
			{
				testpayload.push({"id":arg.id,"url":arg.endPoint});
			}
		});

		var p={
			async: false,
			method: "POST",
			url: this.server.proxyUrl + this.server.urlTestPath,
			data: {
				data: JSON.stringify(testpayload)
			},
			caller: this,
			success: function(data, status, xhr)
			{
				this.caller.testedServices = data;
				this.caller.processTestedServices();
			},
			error: function (request, status, error)
			{
				this.caller.setMessage(error);
			},
			complete: function (request, status, error)
			{
				//
			}
		}

		$.ajax( p );
	}

	processTestedServices()
	{
		Array.from(this.testedServices).forEach(arg =>
		{
			if (arg.status!=200)
			{
				this.services.forEach(function(part, index)
				{
					if (this[index].id==arg.id)
					{
						this[index].disabled=true;
					}
				}, this.services);
			}
		});
	}
}