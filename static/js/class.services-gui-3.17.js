class ServicesGUI extends Base
{
	selector_field_search = "#documentFieldsSearch"
	selector_field_suggestions = "#documentFieldSuggestions"
	selector_field_incl_extras = "#documentFieldsIncludeExtras"
	selector_field_show_all = "#documentFieldsShowAll"
	selector_services = '#services'
	class_field_suggestion_items = ".field-suggestion-items"

	services = []
	queryWindow = null
	placeholder_string = null
	suggestion_search_string = null
	suggestion_matches = []

	constructor(config)
	{
		super(config);
	}

    setServices(services)
    {
        this.services=services;
    }

	setPlaceholderString(placeholder_string)
	{
		this.placeholder_string=placeholder_string;
	}

	setAvailableFields(availableFields)
	{
		this.availableFields=availableFields;
	}

	setIncludeNonSearchableDocumentFields(includeNonSearchableDocumentFields)
	{
		this.includeNonSearchableDocumentFields=includeNonSearchableDocumentFields;
	}

	setQueryWindow(queryWindow)
	{
		this.queryWindow = queryWindow;
	}

    printServices()
    {
        $(this.selector_services).find('option').remove();

        var prevDocument = null;

        for(var i=0;i<this.services.length;i++)
        {
            if (this.services[i].disabled===true)
            {
                continue;
            }

            // this.services[i].index=i;
            var s=this.services[i];

            if (i==0)
            {
                $(this.selector_services).append(this.getTemplate( 'serviceDisabledTpl' ).replace('%LABEL%',s.document));
            }
            else
            if (s.document!=this.services[i-1].document)
            {
                $(this.selector_services).append(this.getTemplate( 'serviceDisabledTpl' ).replace('%LABEL%',''));
                $(this.selector_services).append(this.getTemplate( 'serviceDisabledTpl' ).replace('%LABEL%',s.document));
            }

            if (s.path)
            {
                if (s.document!=prevDocument)
                {
                    var indexKey = s.document;
                }
                else
                {
                    var indexKey = false;
                }

                prevDocument = s.document;

                var title_extras = [];
                var extras = [];
                var query_extra = "";

                if (s.acceptsQueryBody)
                {
                    // extras.push("&percnt;");
                    // title_extras.push("query will be encoded");
                }

                if (s.noQuery)
                {
                    extras.push("&#8211;");
                    title_extras.push("service accepts no query");
                }
                else
                {
                    if (s.acceptsQueryString && !s.acceptsQueryBody)
                    {
                        extras.push("&#128065;");
                        title_extras.push("simple query type");

                        query_extra = "&#65291; name=value" + (s.mandatoryQueryString ? " *" : "");

                    }
                    if (s.acceptsQueryBody)
                    {
                        title_extras.push("advanced query type");
                        query_extra = "&#65291; { query }" + (s.mandatoryQueryBody ? " *" : "");
                    }

                    if (s.mandatoryQueryString || s.mandatoryQueryBody)
                    {
                        title_extras.push("query is mandatory");
                    }
                }

                if (s.explainable)
                {
                    extras.push("&#8265;");
                    title_extras.push("explainable");
                }

                var summary = s.summary ? s.summary + "\n" : "";

                $(this.selector_services).append(
                    this.getTemplate( 'serviceTpl' )
                        .replace('%INDEX%',s.id)
                        .replace('%SELECTED%',(s.default ? ' selected="selected"' : "" ))
                        .replace('%LABEL%',s.label.replace(/\//g," / "))
                        .replace('%SEPARATOR%',query_extra||extras.length > 0 ? this.getTemplate( 'serviceSeparatorTpl' ) : "")
                        .replace('%QUERY%',query_extra)
                        .replace('%EXTRAS%',extras.length > 0 ? this.getTemplate( 'serviceExtrasTpl' ).replace('%EXTRAS%',extras.join("; ")) : "" )
                        // .replace('%LABEL%',s.label && s.label!=s.path ? ' ['+s.label+']' : '' )
                        .replace('%ORIGIN%',(this.usingDefaultRestServiceLists ? this.getTemplate( 'serviceDefaultTpl' ) : "" ))
                        .replace('%INDEX_KEY%',(indexKey ? ' data-index="'+indexKey+'"' : "" ))
                        .replace('%TITLE%',summary + s.label + (title_extras.length > 0 ? "\n("+title_extras.join("; ")+")" : "" ))
                    );
            }
        }

        if ($(this.selector_services + " option").length==0)
        {
            $(this.selector_services).append(this.getTemplate( 'serviceDisabledTpl' ).replace('%LABEL%','found no services'));
        }
    }

    printTypeToFindPlaceholder( str )
    {
        $(this.selector_field_search).attr("placeholder",this.placeholder_string);
    }

	setSuggestionSearchString( suggestion_search_string )
	{
		this.suggestion_search_string=suggestion_search_string;
	}

    setSuggestionMatches()
    {
    	this.suggestion_matches=[];

		// show fields for all document types or just the current, based on selected endpoint
		var findOnlyRelevantFields=$(this.selector_field_show_all).prop('checked')==false;
		var s=this.suggestion_search_string.toLowerCase().replace(/(\s+)/g," ").split(" ");

		var searchableFields=0;
		var search_incl_extras = $(this.selector_field_incl_extras).prop('checked');

        Array.from(this.availableFields).forEach(tF=>
        {
            if (!tF.indexed) return;
            if (findOnlyRelevantFields && this.service && this.service.document!=tF.document) return;

            searchableFields++;

            var found=true;

            for (var j=0;j<s.length;j++)
            {
                var searchme =
                    search_incl_extras ?
                        tF.label + " " +
                            tF.type + " " +
                            (tF.indexed ? "indexed" : "not indexed") + " " +
                            (tF.allowedOperators ?  " " + tF.allowedOperators.join(" ") : "") :
                        tF.searchable;

                if (searchme.toLowerCase().indexOf(s[j])==-1)
                {
                    found=false;
                }
            }

            if (found)
            {
                this.suggestion_matches.push(
                    this.getTemplate("documentFieldSuggestionTpl")
                        .replace('%FIELD%',tF.field)
                        .replace('%LABEL%',tF.label)
                        .replace('%OPERATORS%',tF.allowedOperators ? tF.allowedOperators.join("; ") : "-")
                        .replace('%OPERATOR_GROUPS%',tF.allowedOperators ? this.groupAllowedOperators(tF.allowedOperators).join("; ") : "-")
                        .replace('%TYPE%',tF.type)
                        .replace('%INDEXED%',tF.indexed ? "indexed" : "not indexed" )
                );
            }
        });

		this.setPlaceholderString( searchableFields==0 ? "found no fieldnames" : "type field name" );
		this.printTypeToFindPlaceholder();
    }

    groupAllowedOperators(allowedOperators)
    {
        var groups = [
            { group : 'EQUALS', operators : [ '=','EQUALS','EQUALS_IC','NOT_EQUALS','NOT_EQUALS_IC','!=' ] },
            { group : 'STARTS WITH', operators : [ 'STARTS_WITH','STARTS_WITH_IC','NOT_STARTS_WITH','NOT_STARTS_WITH_IC' ] },
            { group : 'LESS / GREATER THAN', operators : [ '>','GT','>=','GTE','<','LT','<=','LTE' ] },
            { group : 'IN', operators : [ 'IN','NOT_IN' ] },
            { group : 'LIKE', operators : [ 'LIKE','NOT_LIKE' ] },
            { group : 'BETWEEN', operators : [ 'BETWEEN','NOT_BETWEEN' ] },
            { group : 'MATCHES', operators : [ 'MATCHES','NOT_MATCHES' ] },
        ];

        var g = [];

        Array.from(allowedOperators).forEach(allowed=>
        {
            g = g.concat(groups.filter(function(a) { return a.operators.indexOf(allowed)>-1; },allowed));
        });

        return functions.arrayUnique(g.map(x => x.group));
    }

    printSuggestionMatches()
    {
        if (this.suggestion_matches.length==0) return;

		$(this.selector_field_suggestions).html(
			this.getTemplate("documentFieldSuggestionsTpl")
				.replace('%ITEMS%',this.suggestion_matches.join("\n"))
		);

		$(this.selector_field_suggestions)
			.css({
				'position':'absolute',
				'left':  $(this.selector_field_search).offset().left + 'px',
				'top': $(this.selector_field_search).offset().top + $(this.selector_field_search).height() + 'px'
			})
			.toggle(true);

		$(document).on('click',
		{
			caller: this
		},
		function(e)
		{
			$(e.data.caller.selector_field_suggestions).toggle(false);
		});
	}

	setSuggestionClick()
	{
		$(this.class_field_suggestion_items).on('click',
		{
			caller: this
		},
		function(e)
		{
            var field = $(this).attr('data-field');
            field = bindKeys.getCtrlPressed() ? functions.doubleQuoteText(field) : field;
			e.data.caller.queryWindow.insertString(field);
			$(e.data.caller.selector_field_suggestions).toggle(false);
			$(e.data.caller.selector_field_search).val("");
		});
    }

    bindDocumentFieldsSearchKeyUp( )
    {
        $(this.selector_field_search).on('keyup',
			{
				caller: this
			},
	        function(e)
	        {
	            $(e.data.caller.selector_field_suggestions).toggle(false);

	            if ($(this).val().length==0) return;

				e.data.caller.setSuggestionSearchString($(this).val());
				e.data.caller.setSuggestionMatches();
				e.data.caller.printSuggestionMatches();
				e.data.caller.setSuggestionClick();
	        });
    }

	documentFieldsSearchFocus()
	{
		$(this.selector_field_search).focus();
	}

	jumpToServiceIndex(event)
	{
        var index = "?";

        switch(event.which)
        {
          case 83: // s
            index = "specimen";
            break;
          case 84: // t
            index = "taxon";
            break;
          case 71: // g
            index = "geo";
            break;
          case 77: // m
            index = "multimedia";
            break;
          case 68: // d
            index = ".metadata";
            break;
          case 89: // y
            index = ".system";
            break;
          case 86: // v
            index = "static services";
            break;
        }

        if (index=="?")
        {
            return;
        }

		$(this.selector_services+' option[data-index="'+index+'"]').prop('selected', true);
		$(this.selector_services).trigger('change');
	}

    selectServiceByKey(key)
    {
        var s = null;

        Array.from(this.services).forEach(arg=>
        {
            if (arg.key==key)
            {
                s=arg;
            }
        });

        if (s)
        {
            $(this.selector_services).val(s.id).trigger('change');
        }
    }

    selectServiceByLabel(label)
    {
        label = label.replace(/=$/,'')+'=';

        var s = null;

        Array.from(this.services).forEach(arg=>
        {
            if (arg.label==label)
            {
                s=arg;
            }
        });

        if (s)
        {
            $(this.selector_services).val(s.id).trigger('change');
        }
    }

}
