class Query extends Base {

    query = ""
    pathParams = null
    service = null
    isValidQuery = false
    error = ""

    constructor(config)
    {
        super(config);
    }

    setQuery(query)
    {
        this.query = query;
    }

    getQuery()
    {
        return this.query;
    }

    getIsValidQuery()
    {
        return this.isValidQuery;
    }

    getEncodedQuery()
    {
        return functions.rawurlencode(this.query.replace(/(\n|\r)/," ").trim());
    }

    setError(error)
    {
        this.error = error;
    }

    getError()
    {
        return this.error;
    }

    setPathParams(pathParams)
    {
        this.pathParams = pathParams;
    }

    getPathParams()
    {
        return this.pathParams;
    }

    parsePathParams()
    {
        if (this.service.pathParams.length>0)
        {
            // var regex = /\{([^}]*)\}/g;
            // var q = query.match(regex);

            if (this.query.indexOf("{")>-1)
            {
                var query = this.query.substring(this.query.indexOf("{")).trim();
            }
            else
            {
                var query = "";
            }

            var pathParams = this.query.replace(query,'').trim().split("/");

            this.setQuery(query);
            this.setPathParams(pathParams);
        }
        else
        {
            this.setPathParams();
        }
    }

    verifyValidForm()
    {

        try {
            // empty query
            if (this.query.length==0) // && this.service.noQuery
            {
                this.isValidQuery = true;
            }
            else
            // HR query or combined
            if (((this.service.acceptsQueryString && !this.service.acceptsQueryBody) || this.service.pathParams.length>0) &&
                this.query.indexOf('{')==-1 && this.query.indexOf('}')==-1)
            {
                this.isValidQuery = true;
            }
            else
            {
                $.parseJSON( this.query );
                this.isValidQuery = true;
                this.setError("");
            }
        }
        catch(e)
        {
            this.isValidQuery = false;
            this.setError( e.message.replace("SyntaxError:","").replace("JSON.parse:",""), false );
        }
    }
}