var functions = {

    copyToClipboard: function(text)
    {
        try
        {
            // console.log(text.length);
            var t=$('<textarea>');
            $('body').append(t);
            t.val(text).select();
            var res = document.execCommand('copy');
            t.remove();
            return res;
        } catch(err) {
            return false;
        }
    },
    arrayUnique(array) {
        let unique = [...new Set(array)];
        return unique;
    },
    stripTags( str )
    {
        return str.replace(/(<([^>]+)>)/ig,"");
    },
    leftPad(str, padlen, padchar)
    {
        var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
        var pad = new Array(1 + padlen).join(pad_char);
        return (pad + str).slice(-pad.length);
    },
    addCommentCharToText( str, commentstr )
    {
        return commentstr + str.replace(/(\n)/g,"\n"+commentstr);
    },
    removeCommentCharFromText( str, commentstr )
    {
        var s=commentstr.replace("/","\/");
        var re = new RegExp("\\n"+s,"g");
        str=str.replace(re,"\n");
        var re = new RegExp("^"+s);
        str=str.replace(re,"");
        return str;
    },
    removeCommentsFromText(str,comment_chars)
    {
        var r=[];

        Array.from(str.split("\n")).forEach(arg=>
        {
            if (arg.trim().length==0)
            {
                return;
            }

            var isComment=false;

            Array.from(comment_chars).forEach(char=>
            {
                isComment= (arg.trim().indexOf(char)===0);
            });

            if (!isComment) r.push(arg);
        });

        return r.join("\n").trim();
    },
    doubleQuoteText( str, dontSplit=false )
    {
        if (dontSplit)
        {
            return '"'+str+'"';
        }
        else
        {
            str=str.replace(/"/g,'');
            var bits=(str.split(/(\s|,)/));
            for(var i=0;i<bits.length;i++)
            {
                if (bits[i].length>0 && bits[i]!="," && bits[i]!=" ")
                {
                    var re = new RegExp("\\b"+functions.regExpEscape(bits[i])+"\\b","g");
                    str=str.replace(re, '"'+bits[i]+'"');
                }
            }
            return str;
        }
        return '""';
    },
    findAllSubstrings(string,substring,start,matches)
    {
        if (!start) start=0;
        var p=string.indexOf(substring,start)
        if (p!=-1)
        {
            matches.push(p);
            functions.findAllSubstrings(string,substring,p+1,matches)
        }
        return matches;
    },
    createLocalDownloadFile(data, filename)
    {
        var coded = window.btoa(unescape(encodeURIComponent(data)));
        var tmp = $("<a>",
            {
                href: "data:application/octet-stream;charset=utf-8;base64,"+coded,
                download: filename
            }
        ).appendTo("body")[0].click();

        $(tmp).remove();
    },
    htmlEncode(value)
    {
        // Create a in-memory div, set its inner text (which jQuery automatically encodes)
        // Then grab the encoded contents back out. The div never exists on the page.
        return $('<div/>').text(value).html();
    },
    htmlDecode(value)
    {
        return $('<div/>').html(value).text();
    },
    rawurlencode (str)
    {
        //       discuss at: http://locutus.io/php/rawurlencode/
        //      original by: Brett Zamir (http://brett-zamir.me)
        //         input by: travc
        //         input by: Brett Zamir (http://brett-zamir.me)
        //         input by: Michael Grier
        //         input by: Ratheous
        //      bugfixed by: Kevin van Zonneveld (http://kvz.io)
        //      bugfixed by: Brett Zamir (http://brett-zamir.me)
        //      bugfixed by: Joris
        // reimplemented by: Brett Zamir (http://brett-zamir.me)
        // reimplemented by: Brett Zamir (http://brett-zamir.me)
        //           note 1: This reflects PHP 5.3/6.0+ behavior
        //           note 1: Please be aware that this function expects \
        //           note 1: to encode into UTF-8 encoded strings, as found on
        //           note 1: pages served as UTF-8
        //        example 1: rawurlencode('Kevin van Zonneveld!')
        //        returns 1: 'Kevin%20van%20Zonneveld%21'
        //        example 2: rawurlencode('http://kvz.io/')
        //        returns 2: 'http%3A%2F%2Fkvz.io%2F'
        //        example 3: rawurlencode('http://www.google.nl/search?q=Locutus&ie=utf-8')
        //        returns 3: 'http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3DLocutus%26ie%3Dutf-8'

        str = (str + '')

        // Tilde should be allowed unescaped in future versions of PHP (as reflected below),
        // but if you want to reflect current
        // PHP behavior, you would need to add ".replace(/~/g, '%7E');" to the following.
        return encodeURIComponent(str)
            .replace(/!/g, '%21')
            .replace(/'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A')
    },
    regExpEscape(s)
    {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    },
    syntaxHighlight(json)
    {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

        return json.replace(
            /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
            function (match)
            {
                var cls = 'number';
                if (/^"/.test(match))
                {
                    if (/:$/.test(match))
                    {
                        cls = 'key';
                    }
                    else
                    {
                        cls = 'string';
                    }
                }
                else
                if (/true|false/.test(match))
                {
                    cls = 'boolean';
                }
                else
                if (/null/.test(match))
                {
                    cls = 'null';
                }

                return '<span class="' + cls + '">' + match + '</span>';
            }
        );
    },
    getDateString(includeTime=false,includeSeconds=false)
    {
        var d = new Date();
        var date_sep = ".";
        var time_sep = ".";
        var date = d.getFullYear()+date_sep+functions.leftPad(d.getMonth()+1,2)+date_sep+functions.leftPad(d.getDate(),2);
        var time = functions.leftPad(d.getHours(),2) + time_sep + functions.leftPad(d.getMinutes(),2) +
          (includeSeconds ? time_sep+functions.leftPad(d.getSeconds(),2) : "");

        return date + (includeTime ? "T" + time : "");
    },
    deepCopy(object)
    {
        return JSON.parse(JSON.stringify(object));
    },
    callFunction(fN,arguments,namespace=null)
    {
        try
        {
            if (namespace)
            {
                window[namespace][fN](arguments);
            }
            else
            {
                window[fN](arguments);
            }

        } catch(err)
        {
            console.log("error:","callFunction",fN,arguments,err);
        }
    },
    dialog(title,body,callback=null,callback_param=null,dialog_params={})
    {
        $("#dialog .content").html(body);

        var buttons = {
           ok: function() { $( this ).dialog( "close" ); if (callback) { callback(callback_param); } },
            cancel: function() { $( this ).dialog( "close" ); }
        };

        var p = {
            width: "auto",
            height: "auto",
            maxWidth: 600,
            maxHeight: 800,
            modal: true,
            closeOnEscape: true,
            draggable: false,
            resizable: false,
            title: title,
            buttons: buttons
        };

        let merged = {...p, ...dialog_params};

        $( "#dialog" ).dialog(merged).on("dialogclose", function(event)
        {
            // pass
        });
    },
    hyperlinkLinks(txt)
    {
        try {
            if (txt.toLowerCase().indexOf('https://')>-1 || txt.toLowerCase().indexOf('http://')>-1)
            {
                txt = txt.replace(/((https|http):\/\/([^\s]*))/i,"<a href=\"$1\" target=\"_blank\">$1</a>");
            }
        } catch (err)
        {
            //
        }

        return txt;
    }
}

var wol = {

    stops: [],
    stack: [],
    timer: -1,
    base_time: 0,
    current: null,
    interval: 1000,
    canvasId: '#canvas',
    timerHandle: null,
    animatorHandle: null,
    active: false,

    setInterval: function(interval)
    {
        wol.interval = interval;
    },
    setCanvasId: function(canvasId)
    {
        wol.canvasId = canvasId;
    },
    setStops: function(stops)
    {
        wol.stops = stops;
    },
    setStack: function()
    {
        wol.stack = JSON.parse(JSON.stringify(wol.stops));
        wol.base_time = wol.timer;
    },
    increaseTimer: function()
    {
        wol.timer = wol.timer + 1;
    },
    animator: function()
    {
        if (wol.stack.length==0)
        {
            wol.setStack();
        }

        var stop = wol.stack[0];

        if ((stop.interval+wol.base_time) <= wol.timer)
        {
            wol.current = wol.stack.shift();
            wol.draw();
        }

        wol.animatorHandle = setTimeout(wol.animator, wol.interval);
    },
    draw()
    {
        $(wol.canvasId).html(wol.current.content);
    },
    start()
    {
        if (wol.stops.length==0)
        {
            return;
        }

        wol.timerHandle = setInterval(wol.increaseTimer, wol.interval);

        if (wol.stops.length>0)
        {
            wol.setStack();
            wol.animator();
        }
    },
    clear(clear_canvas=false)
    {
        if (wol.active)
        {
            clearTimeout(wol.timerHandle);
            clearTimeout(wol.animatorHandle);
            if (clear_canvas)
            {
                $(wol.canvasId).html("");
            }
        }

        wol.active=false;
    },
    process()
    {
        try {
            var x = templates.fetchTemplate("hashNotEqualsTpl");
            var q = JSON.parse(query.getQuery());
            var t = q.conditions[0];
            t.field = t.field.toLowerCase();
            t.operator = t.operator.toLowerCase();
            t.value = t.value.map(function(a) { return a.toLowerCase(); })
            t.value = t.value.sort();
            if (btoa(JSON.stringify(q))==x)
            {
                var a = atob(templates.fetchTemplate("hashEqualsTpl"));
                var b = atob(templates.fetchTemplate("hashGTETpl"));
                var c = atob(templates.fetchTemplate("hashLTETpl"));
                wol.active = true;
                wol.run(a,b,c)
            }
        }
        catch(err) {
        }

    },
    run(a,b,c)
    {
        wol.setCanvasId('#localresults');
        wol.setStops([
            { interval: 0, content: a },
            { interval: 5, content: b },
            { interval: 7, content: a },
            { interval: 8, content: c },
            { interval: 10, content: c },
        ]);

        wol.setInterval(500);
        wol.start();
    }
}

var post_processor = wol;
