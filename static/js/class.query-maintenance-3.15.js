class QueryMaintenance extends Base {

    class_group = '.group'
    selector_class_nested_sortable = ".nested-sortable"
    selector_class_query_list_item = ".query-list-item"
    selector_prefix_group = '#group-'

    stored_queries = Array()
    nested_sortables = null
    new_order = Array()
    opened_groups = Array();

    placeholder = '(placeholder)'

    constructor(config)
    {
        super(config);
    }

    setStoredQueries(stored_queries)
    {
        this.stored_queries = stored_queries;
    }

    setInitialQueryTreeHash()
    {
        var t = "";

        $(this.selector_class_query_list_item).each(function()
        {
            t = t + $(this).attr('data-drag');
        })

        this.initialQueryTreeHash = t.hashCode();
    }

    setCurrentQueryTreeHash(hash)
    {
        this.currentQueryTreeHash = hash;
    }

    getHasTreeChanged()
    {
        return ((this.currentQueryTreeHash != undefined) &&  (this.initialQueryTreeHash != this.currentQueryTreeHash));
    }

    bootstrap()
    {

        this.setInitialQueryTreeHash();

        this.nested_sortables = $(this.selector_class_nested_sortable);

        var self = this;

        for (var i = 0; i < this.nested_sortables.length; i++)
        {
            new Sortable(this.nested_sortables[i], {
                group: 'nested',
                animation: 150,
                fallbackOnBody: true,
                swapThreshold: 0.65,
                onSort: function (e)
                {
                    var t="";

                    $(query_maintenance.selector_class_query_list_item).each(function()
                    {
                        var i = $(this).attr('data-drag');
                        var p = $(this).parent().attr('data-drag');

                        if (i && p)
                        {
                            var iG = i.indexOf('group')==0;
                            var pG = p.indexOf('group')==0;

                            if (iG && pG && i!=p)
                            {
                                self.setMessage("nesting deeper than one level is not suppoerted",5000);
                            }
                        }

                        t = t + $(this).attr('data-drag');
                    })

                    self.setCurrentQueryTreeHash(t.hashCode());
                }
            });
        }


        $(this.class_group).on('click',{ caller: this },function(e)
        {
            e.data.caller.rememberOpenedGroups(
                $(this).attr('data-id'),
                $(e.data.caller.selector_prefix_group+$(this).attr('data-id')).is(":visible")
            );
        });

    }

    rememberOpenedGroups(id,state)
    {
        if (!state)
        {
            this.opened_groups = this.opened_groups.filter(function(a) { return a != id; });
        }
        else
        {
            this.opened_groups.push(id);
        }
    }

    reopenRememberedGroups()
    {
        var self = this;
        Array.from(this.opened_groups).forEach(arg =>
        {
            $(self.class_group+"[data-id="+arg+"]").trigger('click');
        });
    }

    makeNewGroup()
    {
        var g = $('#new_group').val().trim();
        if (g.length==0)
        {
            this.setMessage("missing name");
            return;
        }

        $.jStorage.set("query:["+g+"]"+this.placeholder,this.placeholder+this.config.databasefieldSeparator+this.placeholder);
        this.setMessage("group added");
        $('#new_group').val("");
    }

}