bindKeys = {

    shiftPressed: false,
    ctrlPressed: false,
    altGrPressed: false,
    codeBitAltKeys: Array,

    doBind()
    {
        // see also:
        // QueryWindow.bootstrapWindow()

        codeBitAltKeys = config.codeBits.map(a => a.altGr).filter(a => a != null);

        $('body').on('keydown',function(e)
        {
            bindKeys.setKeyPressedStateDown(e);
            bindKeys.monitorShiftAltCtrl();
            // replaced with document.onFocus event
            // bindKeys.setCtrlDownTimeOut();

            if (e.ctrlKey && e.which==83) // ctrl+s
            {
                stored_queries.saveQueryViaKeyboard(e);
            }
        });

        $('body').on('keyup',function(e)
        {

            bindKeys.setKeyPressedStateUp(e);
            bindKeys.monitorShiftAltCtrl();

            if (e.which==113 && bindKeys.altGrPressed==false) // F2
            {
                e.preventDefault();
                gui.toggleHelp();
            }
            else
            if (e.which==27) // esc
            {
                e.preventDefault();
                gui.toggleHelp(false);
                gui.toggleResultFullScreen(false);
            }

            if (e.altKey && e.which==46) // alt+delete
            {
                results.clear();
            }
            else
            if (e.ctrlKey && e.which==13) // ctrl+enter
            {
                query_window.triggerQueryClick();
            }
            else
            if (e.ctrlKey && e.which==118) // ctrl+F7
            {
                query_window.clear();
            }
            else
            if (e.ctrlKey && e.which==119) // ctrl+F8
            {
                query_window.focusWindow();
            }
            else
            if (e.ctrlKey && e.which==120) // ctrl+F9
            {
                services_gui.documentFieldsSearchFocus();
            }
            else
            if (bindKeys.getAltGrPressed())
            {
                // AltGr combinations, matching altGr-attributes in config.codeBits
                // console.log(e.which);
                if (codeBitAltKeys.includes(e.which))
                {
                    query_window.insertCodeBit(e.which);
                }
            }
        });

        $(window).on('blur',function()
        {
            bindKeys.depressAllKeys();
        });

    },
    depressAllKeys()
    {
        bindKeys.shiftPressed = false;
        bindKeys.ctrlPressed = false;
        bindKeys.altGrPressed = false;
    },
    monitorShiftAltCtrl()
    {
        $('.indicator').removeClass('active');
        if (bindKeys.getShiftPressed()) $('.indicator.shift').addClass('active');
        if (bindKeys.getAltGrPressed()) $('.indicator.alt').addClass('active');
        if (bindKeys.getCtrlPressed()) $('.indicator.ctrl').addClass('active');
    },
    setKeyPressedStateDown(e)
    {
        if (e.which==16)
        {
            bindKeys.shiftPressed=true;
        }
        if (e.which==17)
        {
            bindKeys.ctrlPressed=true;
        }
        if (e.which==225)
        {
            bindKeys.altGrPressed=true;
        }
    },
    setKeyPressedStateUp(e)
    {
        if (e.which==16) // shift
        {
            bindKeys.shiftPressed=false;
        }
        if (e.which==17) // ctrl
        {
            bindKeys.ctrlPressed=false;
        }
        if (e.which==225) // altGr
        {
            bindKeys.altGrPressed=false;
        }
    },
    getCtrlPressed()
    {
        return bindKeys.ctrlPressed;
    },
    getShiftPressed()
    {
        return bindKeys.shiftPressed;
    },
    getAltGrPressed()
    {
        return bindKeys.altGrPressed;
    },
    /*
    setCtrlDownTimeOut()
    {

        // Ctrl "remains down" if you switch to another window while holding the Ctrl-key down,
        // release it there, and then switch back to Scratchpad. It has no actual state, just up
        // and down events. This function automatically releases Ctrl after having been down for
        // 10 seconds.


        if (bindKeys.getCtrlPressed())
        {
            setTimeout(function()
            {
                bindKeys.ctrlPressed=false;
                bindKeys.monitorShiftAltCtrl();
            },10000)
        }
    }
    */
}