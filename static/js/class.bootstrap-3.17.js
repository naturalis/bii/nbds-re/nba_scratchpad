class Bootstrap extends Base
{
	class_help_topics = '#helpDiv span.help-highlight'
	class_active_comment = '.active-comment-char'
	class_num_of_spaces = '.ctrl-space-number-of-spaces'
	class_direct_path = '.directNbaPath'

	selector_comment_chars = '#exampleCommentChars'
	selector_notes = '#notesDiv'
	selector_help = '#helpDiv'
	selector_notes_drag = '#notesName'
	selector_help_drag = '#helpHeader'
	selector_left_top_div = '#left-top'
	selector_left_bottom = '#left-bottom'
	selector_remoteresults = '#remoteresults'
	selector_localresults = '#localresults'
	selector_export_options = '#exportOptions'
	selector_timer = '#timer'
	selector_hashes = '#hashes'
	selector_save_query = '#save-query'
	selector_query_list = '#query-list'
	selector_query = "#query"
	selector_page = "#page"
	selector_panel_query = "#panel-query"
	selector_panel_query_options = "#panel-query-options"
	selector_panel_url = "#panel-url"
	selector_panel_result_options = "#panel-result-options"
	selector_footer = "#footer"
	selector_top = "#top"
	selector_localresults = "#localresults"
	selector_query_list_header = "#query-list-header"

	query_window_height_inititial = null;
	query_window_height_current = null;
	query_window_height_prev = null;

	keyListenerHandle = null

	constructor(config,server)
	{
		super(config);
		this.setServer(server);
	}

	bootstrapSupportWindows()
	{
		this.bootstrapHelpWindow();

		$(this.selector_comment_chars).html('<code>'+this.config.commentCharacters.join('</code>, <code>')+'</code>');
		$(this.class_active_comment).html(this.config.commentCharacters[this.config.defaultCommentCharIndex]);
		$(this.class_num_of_spaces).html(this.config.ctrlSpaceNumberOfSpaces);
		$(this.selector_notes).draggable({ handle: this.selector_notes_drag });
		$(this.selector_help).draggable({ handle: this.selector_help_drag });
		$(this.class_direct_path).html(this.server.directNbaPath).attr("href",this.server.directNbaPath);
	}

	bootstrapHelpWindow()
	{
		$(this.class_help_topics).each(function()
		{
			$(this).on('mousedown',function()
			{
				// var label=$(this).html().toLowerCase();
				var label=$(this).html();
				$('[title="'+label+'"]').addClass('helpHighlight');
				$('[alt-title="'+label+'"]').addClass('helpHighlight');
			});
			$(this).on('mouseup',function()
			{
				// var label=$(this).html().toLowerCase();
				var label=$(this).html();
				$('[title="'+label+'"]').removeClass('helpHighlight');
				$('[alt-title="'+label+'"]').removeClass('helpHighlight');
			});
		});

		$('#version-history li').on('click',function()
		{
			$(this).find('span').toggle();
		});
	}

	resizeQueryListPanels()
	{
		var a = $(this.selector_page).outerHeight();
		var b = $(this.selector_top).outerHeight();
		var c = $(this.selector_save_query).outerHeight();
		var g = $(this.selector_query_list_header).outerHeight();
		var d = $(this.selector_export_options).is(":visible") ? $(this.selector_export_options).outerHeight() : 0;
		var e = ($(this.selector_timer).is(":visible") ? $(this.selector_timer).outerHeight() : 0);
		var f = ($(this.selector_hashes).is(":visible") ? $(this.selector_hashes).outerHeight() : 0);

		var h = (a-b-c-d-e-f-150);

		h = h < 100 ? 100 : h;

		$(this.selector_query_list).css("height",h+"px");
	}

	startKeyListener()
	{
		this.keyListenerHandle = setInterval(function()
		{
			bindKeys.monitorShiftAltCtrl();
	 	}, 1000);
	}

	stopKeyListener(handle)
	{
		clearInterval(this.keyListenerHandle);
	}

	doLeftPanelsResize()
	{
		this.query_window_height_inititial = $(this.selector_query).css("height");

		var a = $(this.selector_page).outerHeight();
		var b = $(this.selector_panel_query).outerHeight();
		var c = $(this.selector_panel_query_options).outerHeight();
		var d = $(this.selector_panel_url).outerHeight();
		var e = $(this.selector_panel_result_options).outerHeight();
		var f = $(this.selector_footer).outerHeight();
		var g = $(this.selector_top).outerHeight();

		var h = (a-b-c-d-e-f-g-10);

		$(this.selector_localresults).css("height",h+"px");
	}

	bootstrapLeftPanelsResizeTimer()
	{
		this.query_window_height_inititial = $(this.selector_query).css("height");
		this.query_window_height_current = $(this.selector_query).css("height");
		this.query_window_height_prev = this.query_window_height_inititial;
		this.doLeftPanelsResize();
	}

	startLeftPanelsResizeTimer()
	{

		var self = this;

		setInterval(function()
		{
			self.query_window_height_current = $(self.selector_query).css("height");
		},100);

		setInterval(function()
		{
		    if (self.query_window_height_prev!==self.query_window_height_inititial && self.query_window_height_prev==self.query_window_height_current)
		    {
		        self.doLeftPanelsResize();
		    }
		    else
		    {
		        self.query_window_height_prev=self.query_window_height_current;
		    }
		},50);

	}
}