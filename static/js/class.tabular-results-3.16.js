class TabularResults extends Base
{
    selector_result_search = '#resultSearch'
    selector_remote_results = '#remoteresults'
    selector_local_results = '#localresults'
    class_local_results = '.localresults'
    class_tabular_view = '.tabular-view'
    class_json_view = '.json-view'

    currentResultView = null
    localresultsCurrent= null

    result_data = null
    tabular_html = ""
    tsv_data = null
    lines = Array()
    line_index = 0
    headers = Array()
    all_headers = Array()
    max_cols = 100
    header_line_interval = 25
    orientation = "v"
    docs_displayed = 0

    constructor(config)
    {
        super(config);
    }

    getCurrentResultView()
    {
        return this.currentResultView;
    }

    getTabularHeaders()
    {
        var h=[];
        $(".tabular-data th > span:visible").each(function()
        {
            h.push($(this).text().trim());
        })
        let unique = [...new Set(h)];
        h = Array.from(unique).filter( arg => { return arg != "&#8230;" && arg != "…"; });
        h = h.map( arg => arg.replace(/\[(\d)+\]/,"") );
        // console.log(h);
        return h;
    }

    bootstrapWindow()
    {
        this.currentResultView = this.VIEW_TYPE_JSON;
        this.toggleIcons();

        if ((!this.result_data || !this.result_data.localresults) || this.result_data.localresults==this.localresultsCurrent)
        {
            return;
        }

        $(this.class_local_results).toggle(false);
        $(this.selector_remote_results).toggle(true);

        if (!this.result_data.localresults)
        {
            $(this.class_local_results).toggle(false);
            $(this.selector_remote_results).toggle(true);
            this.localresultsCurrent=false;
        }
        else
        {
            $(this.selector_remote_results).off('load');
            $(this.selector_remote_results).toggle(false);
            $(this.class_local_results).toggle(true);
            this.localresultsCurrent=true;
        }
    }

    bootstrapEventHandlers()
    {
        $(".tabular-data th").on("dblclick",function()
        {
            var id = $(this).attr("data-col");
            $('span[data-col='+id+']').toggle();
        })

        $(".tabular-data th").on("click",function()
        {
            if (!bindKeys.getCtrlPressed())
            {
                return;
            }

            var id = $(this).attr("data-col");
            var this_class = "tabular-cell-highlight";

            $('td[data-col='+id+']').each(function()
            {
                if ($(this).hasClass(this_class))
                {
                    $(this).removeClass(this_class);
                }
                else
                {
                    $(this).addClass(this_class);
                }
            })
        })
    }

    toggleIcons()
    {
        $(this.class_tabular_view).toggle(true);
        $(this.class_json_view).toggle(false);
        $(this.selector_result_search).prop("disabled",false)

        if (this.currentResultView==this.VIEW_TYPE_JSON)
        {
            $(this.class_tabular_view).toggle(true);
            $(this.class_json_view).toggle(false);
            $(this.selector_result_search).prop("disabled",false)
        }
        else
        {
            $(this.class_tabular_view).toggle(false);
            $(this.class_json_view).toggle(true);
            $(this.selector_result_search).prop("disabled",true)
        }
    }

    changeResultView()
    {
        if (this.currentResultView==this.VIEW_TYPE_JSON)
        {
            this.flattenResultSet();
            this.makeTabularHtml();
            this.tabularResultView();
            this.bootstrapEventHandlers();
            this.currentResultView=this.VIEW_TYPE_TABULAR;
        }
        else
        {
            main.switchToJSONResultView();
            this.currentResultView=this.VIEW_TYPE_JSON;
        }
        this.toggleResultViewIcons();
    }

    resetData()
    {
        this.result_data=null;
        this.headers=Array();
        this.all_headers=Array();
        this.lines=Array();
        this.line_index=0;
        this.tabular_html="";
        this.currentResultView=this.VIEW_TYPE_JSON;
        this.tsv=null;
        this.orientation = "v";
        this.toggleResultViewIcons();
    }

    setResultData(result_data)
    {
        this.result_data=result_data;
    }

    toggleResultViewIcons()
    {
        if (this.currentResultView==this.VIEW_TYPE_JSON)
        {
            $('.tabular-view').toggle(true);
            $('.json-view').toggle(false);
            $('#resultSearch').prop("disabled",false)
        }
        else
        {
            $('.tabular-view').toggle(false);
            $('.json-view').toggle(true);
            $('#resultSearch').prop("disabled",true)
        }
    }

    flattenResultSet()
    {
        if (this.all_headers.length>0 && this.headers.length>0)
        {
            return;
        }

        if (this.result_data.data.resultSet)
        {
            var data = this.result_data.data.resultSet;
        }
        else
        if (Array.isArray(this.result_data.data))
        {
            var data = this.result_data.data;
        }
        else
        {
            var data = [this.result_data.data];
        }

        this.all_headers=[];
        this.headers=[];

        this.doFlatten(data,true);
        this.all_headers=[...this.headers];
        this.headers=this.headers.slice(0,this.max_cols);
        // console.log(this.all_headers,this.headers);
        this.doFlatten(data);
    }

    doFlatten(result_set,headers_only=false)
    {
        if(!Array.isArray(result_set[0]) && typeof result_set[0] != "object")
        {
            result_set = [result_set];
        }

        Array.from(result_set).forEach(result =>
        {
            if (!headers_only) this.lines[this.line_index]=[];
            var doc = result.item ? result.item : result;
            this.flattenDoc(doc,null,headers_only);
            if (!headers_only) this.line_index++;
        });

        if (headers_only)
        {
            let unique = [...new Set(this.headers)];
            this.headers = unique;
        }
    }

    flattenDoc(obj,parent=null,headers_only=false)
    {
        for (var prop in obj)
        {
            if (Object.prototype.hasOwnProperty.call(obj, prop))
            {
                if (Array.isArray(obj[prop]))
                {
                    Array.from(obj[prop]).forEach(function(arg,idx)
                    {
                        {
                            var key = (parent ? parent + "." : "") + prop + "["+idx+"]";

                            if (
                                (key.indexOf("shape.coordinates")!=-1) ||
                                (key.indexOf("taxonomicEnrichments")!=-1)
                            )
                            {
                                return;
                            }

                            if (typeof arg === 'object' && arg !== null)
                            {
                                this.flattenDoc(arg,key,headers_only);
                            }
                            else
                            {
                                if (headers_only)
                                {
                                    this.headers.push(key);
                                }
                                else
                                {
                                    this.lines[this.line_index].push({ "key" : key, "val" : arg});
                                }
                            }
                        }
                    },this)
                }
                else
                {
                    var key = (parent ? parent + "." : "") + prop;

                    if (key.indexOf("taxonomicEnrichments")>0)
                    {
                        return;
                    }

                    if (typeof obj[prop] === 'object' && obj[prop] !== null)
                    {
                        this.flattenDoc(obj[prop],key,headers_only);
                    }
                    else
                    {
                        if (headers_only)
                        {
                            this.headers.push(key);
                        }
                        else
                        {
                            this.lines[this.line_index].push({ "key" : key, "val" : obj[prop]});
                        }
                    }
                }
            }
        }
    }

    makeTsvData()
    {
        this.tsv_data = null;

        var buffer=[];
        var tmp=[];
        var sep="\t";

        Array.from(this.all_headers).forEach(function(header,idx)
        {
            tmp.push(header);
        });

        buffer.push(tmp.join(sep));

        var all_headers  = this.all_headers;

        Array.from(this.lines).forEach(function(line,line_counter)
        {
            tmp=[];

            Array.from(all_headers).forEach(function(header,idx)
            {
                var hKey = Array.from(line).find(cell =>
                {
                    return header==cell.key;
                });
                if (!hKey)
                {
                    tmp.push("");
                }
                else
                {
                    tmp.push(hKey.val);
                }
            },all_headers);

            buffer.push(tmp.join(sep));
        });

        this.tsv_data = buffer.join("\n");
    }

    getTsvData()
    {
        return this.tsv_data;
    }

    makeTabularHtml()
    {
        var caller = this;
        var data_cells=[];
        var header_cells=[];

        Array.from(this.lines).forEach(function(line,line_counter)
        {
            if (this.result_data.isDisplayedCapped && line_counter>=this.result_data.docsDisplayed)
            {
                return;
            }

            data_cells[line_counter]=[];

            Array.from(caller.headers).forEach(function(header,idx)
            {
                var hKey = Array.from(line).find(cell =>
                {
                    return header==cell.key;
                });
                if (!hKey)
                {
                    // data_cells[line_counter].push(tpl.replace("%DATA%","&nbsp;").replace(/%COL%/g,idx));
                    data_cells[line_counter].push({ "data": "&nbsp;", "idx" : idx });
                }
                else
                {
                    // data_cells[line_counter].push(tpl.replace("%DATA%",hKey.val).replace(/%COL%/g,idx));
                    data_cells[line_counter].push({ "data": hKey.val, "idx" : idx });
                }
            });

        },caller);

        var buffer=[];
        var tmp = [];
        var cTpl = this.getTemplate("tabularViewCell_tpl");
        var rTpl = this.getTemplate("tabularViewRow_tpl");
        var nTpl = this.getTemplate("tabularNoticeCell_tpl");

        // notice if total number of columns overshoots the max
        // not: this also applies
        var notice = this.all_headers.length>this.headers.length ?
            this.getTemplate("tabularColumnMessage_tpl")
                .replace("%FIRST%",this.headers.length)
                .replace("%ALL%",this.all_headers.length) :
            null;

        var cap_notice = this.result_data.isDisplayedCapped ?
            this.getTemplate("maxDisplayedMessageTpl")
                .replace("%CAP%",this.result_data.docsDisplayed)
                .replace("%ALL%",this.result_data.data.resultSet.length) :
            null;


        if (this.orientation=="v")
        {

            var hTpl = this.getTemplate("tabularViewHeaderCell_tpl");

            Array.from(this.headers).forEach(function(header,idx)
            {
                tmp.push(hTpl.replace(/%DATA%/g,header).replace(/%COL%/g,idx));
            });

            var repeating_header_line = rTpl.replace("%DATA%",tmp.join("\t"));

            if (notice)
            {
                tmp.push(cTpl.replace("%DATA%",notice).replace(/%COL%/g,""));
            }

            buffer.push(rTpl.replace("%DATA%",tmp.join("\t")));
            tmp = []

            Array.from(data_cells).forEach(function(row,line_counter)
            {
                Array.from(row).forEach(function(cell)
                {
                    tmp.push(cTpl.replace("%DATA%",functions.hyperlinkLinks(cell.data)).replace(/%COL%/g,cell.idx));
                })

                buffer.push(rTpl.replace("%DATA%",tmp.join("\t")));
                tmp = []

                if ((line_counter+1) % caller.header_line_interval == 0)
                {
                    buffer.push(repeating_header_line);
                }

            },caller);

            if (cap_notice)
            {
                buffer.push(rTpl.replace("%DATA%",nTpl.replace("%DATA%",cap_notice).replace("%COLSPAN%",this.headers.length)));
            }

        }
        else
        {
            var hTpl = this.getTemplate("tabularViewVerticalHeaderCell_tpl");

            Array.from(this.headers).forEach(function(header,idx)
            {
                tmp.push(hTpl.replace(/%DATA%/g,header).replace(/%COL%/g,idx));

                var col_count = 0;

                Array.from(data_cells).forEach(function(row)
                {
                    Array.from(row).forEach(function(cell,cell_idx)
                    {
                        if (cell_idx==idx)
                        {
                            tmp.push(cTpl.replace("%DATA%",functions.hyperlinkLinks(cell.data)).replace(/%COL%/g,cell.idx));
                            col_count++;

                            if (col_count % caller.header_line_interval == 0)
                            {
                                tmp.push(hTpl.replace(/%DATA%/g,header).replace(/%COL%/g,idx));
                            }
                        }
                    })
                });

                if (idx==0 && cap_notice)
                {
                    tmp.push(nTpl.replace("%DATA%",cap_notice).replace("%COLSPAN%","1"));
                }

                buffer.push(rTpl.replace("%DATA%",tmp.join("\t")));
                tmp = []

            },caller);

            if (notice)
            {
                buffer.push(cTpl.replace("%DATA%",notice).replace(/%COL%/g,""));
            }

        }

        this.tabular_html = this.getTemplate("tabularViewTable_tpl").replace("%DATA%",buffer.join("\n"));
    }

    tabularResultView()
    {
        var table = this.getTemplate( 'responseTabularTpl' ).replace('%RESPONSE%',this.tabular_html);
        $( this.selector_local_results ).html(table);
    }

    rotate()
    {
        if (this.currentResultView!=this.VIEW_TYPE_TABULAR)
        {
            return;
        }

        this.orientation = this.orientation == "h" ? "v" : "h";
        this.makeTabularHtml();
        this.tabularResultView();
    }
}
