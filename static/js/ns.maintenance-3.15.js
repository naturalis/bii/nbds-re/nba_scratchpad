var maintenance = {

    saveNewQueryOrder(e)
    {
        $(query_maintenance.selector_class_query_list_item).each(function()
        {
            var i = $(this).attr('data-drag');
            var p = $(this).parent().attr('data-drag');
            var queryId = $(this).attr('data-id');
            var groupId = $(this).parent().attr('data-group');

            if (i && p)
            {
                var iG = i.indexOf('group')==0;
                var pG = p.indexOf('group')==0;

                if (iG && pG && i!=p)
                {
					// scratchpad.setMessage("one level only!");
                }
                else
                {
                    if (queryId)
                    {
                        var query_key_old = query_maintenance.stored_queries[queryId];

                        if (!query_key_old)
                        {
                            return;
                        }

                        var query_key_new = query_key_old.replace(/^query:/,"");
                        var matches = query_key_new.match(query_maintenance.config.queryGroupRegExp);
                        if (matches)
                        {
                            query_key_new = query_key_new.replace(matches[0],'').trim();
                        }

                        query_key_new = "query:" + (groupId ? "["+groupId+"] " : "" ) + query_key_new;

                        var query=query_maintenance.getStoredItem(query_key_old);

                        $.jStorage.deleteKey(query_key_old);

                        var t = query.split(query_maintenance.config.databasefieldSeparator);

                        if (t[0]==query_maintenance.placeholder && t[1]==query_maintenance.placeholder)
                        {
                        	return;
                        }

                        $.jStorage.set(query_key_new,query);
                        // console.log("saved");
                    };
                }
            }
        })

        query_maintenance.setMessage("new order saved");

		maintenance.refreshQueryTree();

    },
    refreshQueryTree()
    {
        stored_queries.setQueries();
        stored_queries.printList();
        query_maintenance.setStoredQueries(stored_queries.getQueries());
        query_maintenance.bootstrap();
        query_maintenance.setCurrentQueryTreeHash(null);
        query_maintenance.reopenRememberedGroups()
    },
    showQuery(number)
    {
        var t = stored_queries.getQuery(number);
        if (!t)
        {
        	return;
        }

        var s = services.getServices().filter(function(x){ return x.key==t[1]; });
        var n = stored_queries.getNote( stored_queries.queries[number] )
        var label = stored_queries.queries[number].replace(stored_queries.storage_query_prefix,"");

        functions.dialog(
            label,
            query_maintenance.getTemplate("queryExampleTpl")
            	.replace("%SERVICE%", s.length>0 ? s[0].label : "?")
            	.replace("%QUERY%",t[0])
            	.replace("%NOTE%",n),
            null,
            null,
            {
                width : "60%",
                buttons: [ { text: "close", click: function() { $( this ).dialog( "close" ); } } ],
                height: "auto"
            }
        );

    },
    updateGroupName(ele)
    {
        var data_id = $(ele).attr('data-id');
        var old_group_name = $('input[type=hidden][data-id='+data_id+']').val();
        var new_group_name = $('input[type=text][data-id='+data_id+']').val().trim();

        if (new_group_name.length==0)
        {
        	return;
        }

        $(query_maintenance.selector_class_query_list_item).each(function()
        {
            var queryId = $(this).attr('data-id');
            var groupId = $(this).parent().attr('data-group');
            var query_key_old = query_maintenance.stored_queries[queryId];

            if (!query_key_old)
            {
            	return;
            }

            var query=query_maintenance.getStoredItem(query_key_old);

            if (groupId && groupId==old_group_name)
            {
                $.jStorage.deleteKey(query_key_old);
                $.jStorage.set(query_key_old.replace("["+old_group_name+"]","["+new_group_name+"]"),query);

                // TODO
				// var index = query_maintenance.opened_groups.indexOf(old_group_name.hashCode());
				// console.log(index,query_maintenance.opened_groups,old_group_name.hashCode());
				// if (index !== -1)
				// {
				//     query_maintenance.opened_groups[index] = new_group_name.hashCode();
				// }
            }
            else
            {
                $.jStorage.deleteKey(query_key_old);
                $.jStorage.set(query_key_old,query);
            }
        })

        query_maintenance.setMessage("group name saved");
		maintenance.refreshQueryTree();
    },
    updateQueryName(ele)
    {
        var data_id = $(ele).attr('data-id');
        var old_query_name = $('input[type=hidden][data-id='+data_id+']').val();
        var new_query_name = $('input[type=text][data-id='+data_id+']').val().trim();

        if (new_query_name.length==0 || old_query_name==new_query_name)
        {
        	return;
        }

        $(query_maintenance.selector_class_query_list_item + " > .list-query-item").each(function()
        {
            var this_data_id = $(this).attr('data-id');

            var key = query_maintenance.stored_queries[this_data_id];
            var query = query_maintenance.getStoredItem(key);

            if (data_id.replace("query-","")==this_data_id)
            {
                $.jStorage.deleteKey(key);
                $.jStorage.set("query:"+new_query_name,query);
            }
            else
            {
                $.jStorage.deleteKey(key);
                $.jStorage.set(key,query);
            }
        })

        query_maintenance.setMessage("query name saved");
		maintenance.refreshQueryTree();
    }

}
