import os, time
import requests, urllib.request, urllib.parse, urllib.error, json, http.client
from flask import Flask, request, make_response, render_template, stream_with_context, Response
from urllib.parse import unquote_plus
# from flask_cors import CORS
from flask_compress import Compress
from html.parser import HTMLParser

# PRIVATE_NBA_ADDRESS is the docker-address of the NBA-container, including port, excluding protocol
private_nba_address = os.getenv('PRIVATE_NBA_ADDRESS','nba-api')
# PUBLIC_NBA_ADDRESS is the regular NBA-address, including port, excluding protocol
public_nba_address = os.getenv('PUBLIC_NBA_ADDRESS','api.biodiversitydata.nl')
nba_version = os.getenv('NBA_VERSION','v2')
nba_request_timeout = int(os.getenv('NBA_TIMEOUT','10'))
listener_base_path = os.getenv('LISTENER_BASE_PATH','/scratchpad').rstrip('/')
force_get_requests = os.getenv('FORCE_GET_REQUESTS','false').lower()=='true'

# internal docker communication is http
base_url = 'http://' + private_nba_address

# version is parsed from the help.html template
current_version = "v0"

# for html-links within scratchpad
scratchpad_path = listener_base_path+'/'

app = Flask(__name__, static_url_path=listener_base_path+"/static")
Compress(app)




class HTMLParser(HTMLParser):
	started = False
	this_id = 'version-history'
	lastest_version = "v0"

	def handle_starttag(self, tag, attrs):
		attrs = dict(attrs)
		if 'id' in attrs and attrs['id'] == self.this_id:
			self.started = True
		if self.started:
			if 'class' in attrs:
				self.lastest_version = attrs['class'].strip()
				self.started = False

	def get_latest_version(self):
		return self.lastest_version

parser = HTMLParser()
f = open("templates/help.html", "r")
contents =f.read()
parser.feed(contents)
current_version = parser.get_latest_version()


def print_settings():
	print(("current_version: {}".format(current_version)))
	print(("private_nba_address: {}".format(private_nba_address)))
	print(("public_nba_address: {}".format(public_nba_address)))
	print(("nba_version: {}".format(nba_version)))
	print(("nba_request_timeout: {}".format(nba_request_timeout)))
	print(("listener_base_path: {}".format(listener_base_path)))
	print(("force_get_requests: {}".format(force_get_requests)))


@app.route(listener_base_path + '/', methods=['GET'])
def root_request():
	query=request.args.get('q', '')
	service_key=request.args.get('k', '')
	execute_query = 'true' if request.args.get('e', '')=='1' else 'false'
	strip_url = 'true' if request.args.get('s', '')=='1' else 'false'

	return render_template('index.html',
		nba_address=public_nba_address,
		private_nba_address=private_nba_address,
		nba_version=nba_version,
		query=query,
		execute_query=execute_query,
		service_key=service_key,
		strip_url=strip_url,
		scratchpad_path=scratchpad_path)

@app.route(listener_base_path + '/maintenance', methods=['GET','POST'])
def maintenance_request():
	return render_template('maintenance.html',
		nba_address=public_nba_address,
		private_nba_address=private_nba_address,
		nba_version=nba_version,
		scratchpad_path=scratchpad_path)

@app.route(listener_base_path + '/url_test', methods=['POST'])
def url_test_request():
	s = []
	try:
		if 'data' in request.form:
			for value in json.loads(request.form['data']):
				this_url = "{}/{}{}".format(base_url,nba_version,value['url'])
				r = requests.head(url=this_url,timeout=nba_request_timeout, allow_redirects=True)
				s.append({"id":value['id'],"status":r.status_code})
	except Exception as e:
		pass

	response = make_response(json.dumps(s))
	response.headers['content-type'] = "application/json"
	return response


@app.route(listener_base_path + '/proxy', methods=['GET','POST'])
def proxy_request():
	if request.method == "POST" and 'method' in request.form:
		this_method = request.form['method']
	else:
		this_method = request.method

	payload = {}

	if 'data' in request.form:

		for name, value in json.loads(request.form['data']).items():
			try:
				payload[name] = unquote_plus(value)
			except Exception as e:
				payload[name] = value

	explain_level = False

	if '__explain' in request.form:
		if json.loads(request.form['__explain']):
			explain_level = json.loads(request.form['__explain'])
	try:
		this_url = base_url+request.form['service']+("?__explain&__level="+explain_level if explain_level else "")

		if this_method == 'POST' and not force_get_requests == True:

			# if len(payload)==0:
			# 	headers = {'content-type': 'application/json'}
			# else:
			# 	headers = {}

			headers = {
				"Content-Type": "application/json",
				"User-Agent": "NBA Scratchpad {}".format(current_version)
			}

			if "_querySpec" in payload:
				payload = payload["_querySpec"]

			if isinstance(payload, str):
				payload = payload.encode('utf-8')

			r = requests.post(url=this_url,data=payload,timeout=nba_request_timeout,headers=headers)
		else:
			r = requests.get(url=this_url,params=payload,timeout=nba_request_timeout)

		# print(r.request.headers)

		response = make_response(r.content)
		response.headers['content-type'] = r.headers.get('content-type')

		# for header, value in r.headers.iteritems():
		# 	print(header, value)

		# errors other than 300 and 400's are sent to the browser as-is
		if 300 <= r.status_code < 500:
			if not (r.text == None or r.text==""):
				s = r.text.rstrip().replace("\n",": ")
			else:
				s = "{} ({})".format(str(r.status_code),http.client.responses[r.status_code])
			raise ValueError(s)

		if (r.headers.get('content-type')=='application/zip'):
			response.headers['content-disposition'] = r.headers.get('content-disposition')

		return response

	except Exception as e:
		try:
			payload = payload.decode("utf-8")
		except (UnicodeDecodeError, AttributeError):
			pass

		err = { 'scratchpad_proxy_error' :
				{
					'error' : str(e),
					'nba_url' : this_url.replace(private_nba_address,public_nba_address),
					'payload' : payload,
					'method' : this_method,
					'force_get_requests' : force_get_requests
				}
			}
		response = make_response(json.dumps(err))
		response.headers['content-type'] = "application/json"
		return response


@app.route('/', methods=['GET'])
def base():
	return render_template('root.html',scratchpad_path=scratchpad_path)


if __name__ == '__main__':
	FLASK_PORT = int(os.getenv('FLASK_PORT',80))
	print_settings()
	app.run(debug=(os.getenv('FLASK_DEBUG')=="1"),host='0.0.0.0', port=FLASK_PORT)
